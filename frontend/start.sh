#!/usr/bin/env bash

scriptdir="$(dirname "$0")"
cd "$scriptdir"

npm ci
npm run start