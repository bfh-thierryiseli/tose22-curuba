import { render, html } from "lit";

export class ComponentBase extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.render()
    }

    render() {
        render(this.getTemplate(), this)
    }

    getTemplate() {
        return html``
    }
}