import "./views/header/HeaderView";
import "./views/home/HomeView";
import "./views/customer-home/CustomerHomeView";
import "./views/customer-form/CustomerFormView";
import "./views/customer-selection/CustomerSelectionView";
import "./views/customer-detail/CustomerDetailView";
import "./views/customer-reservation/CustomerReservationView";
import "./views/artist-home/ArtistHomeView";
import "./views/artist-availability/ArtistAvailabilityView";
import "./views/artist-selection/ArtistSelectionView";
import "./views/artist-form/ArtistFormView";
import "./views/artist-detail/ArtistDetailView";
import "./views/artist-reservation/ArtistReservationView";
import "./views/artist-contact-form/ArtistContactFormView";
import "./views/artist-contact-success/ArtistContactSuccessView";
import "./views/artist-rating-form/ArtistRatingFormView";
import "./views/reservation-form/ReservationFormView";
import "./views/reservation-detail/ReservationDetailView";
import "./views/logout-success/LogoutSuccessView";
import "@shoelace-style/shoelace/dist/shoelace.js";
import "@shoelace-style/shoelace/dist/themes/light.css";

import { Router } from "@vaadin/router";
import { registerIconLibrary } from "@shoelace-style/shoelace/dist/utilities/icon-library.js";

if (!window.validate) {
  window.validate = (formTarget) => {
    let valid = true;
    formTarget.querySelectorAll("sl-input").forEach((input) => {
      input.removeAttribute("init");
      if (input.shadowRoot.querySelector(".input--invalid")) {
        valid = false;
      }
    });
    formTarget.querySelectorAll("sl-textarea").forEach((textarea) => {
      textarea.removeAttribute("init");
      if (textarea.shadowRoot.querySelector(".textarea--invalid")) {
        valid = false;
      }
    });
    formTarget.querySelectorAll("sl-select").forEach((select) => {
      select.removeAttribute("init");
      if (select.shadowRoot.querySelector(".select--invalid")) {
        valid = false;
      }
    });
    return valid;
  };
}

if (!window.showAlert) {
  window.showAlert = (variant, innerHTML) => {
    const alert = Object.assign(document.createElement("sl-alert"), {
      variant: variant,
      closable: true,
      duration: 5000,
      innerHTML: innerHTML,
    });
    document.body.append(alert);
    alert.toast();
  };
}

if (!window.dateToISOString) {
  window.dateToISOString = (date) => {
    var tzo = -date.getTimezoneOffset(),
      dif = tzo >= 0 ? "+" : "-",
      pad = function (num) {
        return (num < 10 ? "0" : "") + num;
      };

    return (
      date.getFullYear() +
      "-" +
      pad(date.getMonth() + 1) +
      "-" +
      pad(date.getDate()) +
      "T" +
      pad(date.getHours()) +
      ":" +
      pad(date.getMinutes()) +
      ":" +
      pad(date.getSeconds()) +
      dif +
      pad(Math.floor(Math.abs(tzo) / 60)) +
      ":" +
      pad(Math.abs(tzo) % 60)
    );
  };
}

registerIconLibrary("fa", {
  resolver: (name) => {
    const filename = name.replace(/^fa[rbs]-/, "");
    let folder = "regular";
    if (name.substring(0, 4) === "fas-") folder = "solid";
    if (name.substring(0, 4) === "fab-") folder = "brands";
    return `https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.1/svgs/${folder}/${filename}.svg`;
  },
  mutator: (svg) => svg.setAttribute("fill", "currentColor"),
});

window.keycloak = {};
fetch("./config/config.json", { cache: "reload" })
  .then((res) => res.json())
  .then((config) => {
    import(config.ssoKeycloakAdapter /* @vite-ignore */).then(() => {
      window.keycloak = new Keycloak({
        url: config.ssoUrl,
        realm: config.ssoRealm,
        clientId: config.ssoClient,
      });
      window.keycloak
        .init({
          onLoad: "check-sso",
          promiseType: "native",
          checkLoginIframe: false,
          pkceMethod: "S256",
        })
        .then((initialized) => {
          document.querySelector("#app").innerHTML = `
            <header-view></header-view>
            <main></main>
          `;
          const router = new Router(document.querySelector("main"));
          router.setRoutes([
            {
              path: "/",
              component: "home-view",
            },
            {
              path: "/customer",
              component: "customer-home-view",
            },
            {
              path: "/customer-detail",
              component: "customer-detail-view",
            },
            {
              path: "/customer-form",
              component: "customer-form-view",
            },
            {
              path: "/customer-selection",
              component: "customer-selection-view",
            },
            {
              path: "/customer-reservation",
              component: "customer-reservation-view",
            },
            {
              path: "/artist",
              component: "artist-home-view",
            },
            {
              path: "/artist-availability",
              component: "artist-availability-view",
            },
            {
              path: "/artist-reservation",
              component: "artist-reservation-view",
            },
            {
              path: "/artist-detail",
              component: "artist-detail-view",
            },
            {
              path: "/artist-selection",
              component: "artist-selection-view",
            },
            {
              path: "/artist-rating-form",
              component: "artist-rating-form-view",
            },
            {
              path: "/reservation-form",
              component: "reservation-form-view",
            },
            {
              path: "/reservation-detail",
              component: "reservation-detail-view",
            },
            {
              path: "/artist-contact-form",
              component: "artist-contact-form-view",
            },
            {
              path: "/artist-contact-success",
              component: "artist-contact-success-view",
            },
            {
              path: "/artist-form",
              component: "artist-form-view",
            },
            {
              path: "/logout-success",
              component: "logout-success-view",
            },
          ]);
        })
        .catch((error) => {
          console.log("error", error);
        });
      window.keycloak.onTokenExpired = () => {
        keycloak.updateToken(30).success();
      };
    });
  });
