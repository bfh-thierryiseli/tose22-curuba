import { ServiceBase } from '../ServiceBase';

export default class RatingService extends ServiceBase {
    constructor() {
        super('ratings/public')
    }
}
