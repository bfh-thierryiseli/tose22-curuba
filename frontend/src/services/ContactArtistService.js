import { ServiceBase } from '../ServiceBase';

export default class ContactArtistService extends ServiceBase {
    constructor() {
        super('contact/artist')
    }
}
