import { ServiceBase } from '../ServiceBase';

export default class ReservationService extends ServiceBase {
    constructor() {
        super('reservations')
    }
}
