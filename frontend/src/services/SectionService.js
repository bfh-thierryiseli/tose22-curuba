import { ServiceBase } from '../ServiceBase';

export default class SectionService extends ServiceBase {
    constructor() {
        super('sections')
    }
}