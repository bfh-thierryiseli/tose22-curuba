import { ServiceBase } from '../ServiceBase';

export default class CustomerService extends ServiceBase {
    constructor() {
        super('customers')
    }
}
