import { ServiceBase } from '../ServiceBase';

export default class ArtistPublicService extends ServiceBase {
    constructor() {
        super('artists/public')
    }
}
