import { ServiceBase } from '../ServiceBase';

export default class ArtistService extends ServiceBase {
    constructor() {
        super('artists')
    }
}
