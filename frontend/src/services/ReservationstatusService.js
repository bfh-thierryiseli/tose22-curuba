import { ServiceBase } from '../ServiceBase';

export default class ReservationstatusService extends ServiceBase {
    constructor() {
        super('reservations/reservationstatus')
    }
}
