import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import EntranceImage from "../../images/entrance.jpg";

class HomeView extends ComponentBase {
  constructor() {
    super();
  }

  getTemplate() {
    return html`
      <style>
        .entrance {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          min-height: 100%;
          background: no-repeat center center;
          background-image: url("${EntranceImage}");
          background-size: cover;
        }

        .entrance-content {
          background-color: rgba(0, 0, 0, 0.35);
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          min-height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-wrap: wrap;
          text-align: center;
        }

        .main-title {
          font-size: 5rem;
          margin: 0;
          color: #fff;
          flex: 100%;
        }
      </style>
      <div class="entrance"></div>
      <div class="entrance-content">
        <div>
          <h1 class="main-title">click & book</h1>
          ${this.getArtistEntrace()}
          <sl-button href="customer" variant="neutral"
            >Ich bin Interessent</sl-button
          >
        </div>
      </div>
    `;
  }

  getArtistEntrace() {
    if (keycloak.authenticated) {
      return html`<sl-button href="artist-reservation" variant="neutral"
        >Ich bin Künstler</sl-button
      >`;
    }
    return html`<sl-button href="artist" variant="neutral"
      >Ich bin Künstler</sl-button
    >`;
  }
}

customElements.define("home-view", HomeView);
