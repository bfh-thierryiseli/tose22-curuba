import { html, css } from "lit";
import { ComponentBase } from "../../ComponentBase";
import UserService from "../../services/UserService";
import EntranceImage from "../../images/entrance.jpg";

class HeaderView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    this.settings = {
      defaultCustomer: {},
      defaultArtist: {},
    };
    if (keycloak.authenticated) {
      this.userService = new UserService();
      this.userService
        .list("", `${keycloak.tokenParsed.preferred_username}/artists`)
        .then((userArtists) => {
          if (userArtists.length < 1) {
            this.noArtistProfile = true;
          }
          this.userService
            .list("", `${keycloak.tokenParsed.preferred_username}/customers`)
            .then((userArtists) => {
              if (userArtists.length < 1) {
                this.noCustomerProfile = true;
              }
              this.userService
                .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
                .then((settings) => {
                  if (settings.length > 0) {
                    this.settings = settings[0];
                  }
                  if (
                    this.settings.defaultCustomer === null ||
                    this.settings.defaultCustomer === undefined
                  ) {
                    this.settings.defaultCustomer = {};
                  }
                  if (
                    this.settings.defaultArtist === null ||
                    this.settings.defaultArtist === undefined
                  ) {
                    this.settings.defaultArtist = {};
                  }
                  this.render();
                });
            });
        });
    } else {
      this.render();
    }
  }

  getTemplate() {
    return html`
      <style>
        .header {
          position: relative;
          min-height: 100px;
        }

        .header-image {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background: no-repeat center center;
          background-image: url(${EntranceImage});
          background-size: cover;
        }

        .header-content {
          background-color: rgba(0, 0, 0, 0.6);
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          min-height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-wrap: wrap;
          text-align: center;
          color: #fff;
          font-weight: bold;
          font-size: 2rem;
        }

        .header-content > div > a {
          text-decoration: none;
          color: #fff;
          margin: 0 -0.25rem;
          padding: 0 0.25rem;
          transition: all 0.4s ease-in-out;
        }

        .header-content > div > a:hover {
          font-size: 2.5rem;
        }

        .header-content small {
          font-size: 16px;
        }

        .menu {
          text-align: center;
        }

        .menu > span {
          display: block;
          margin-top: 0.5rem;
        }

        .menu > * {
          margin-top: 0.5rem;
        }

        sl-alert {
          margin: 1rem 0;
        }
      </style>
      <div class="header">
        <div class="header-image"></div>
        <div class="header-content">
          <div>
            <a href="/"> click & book </a>
          </div>
        </div>
      </div>
      <div class="content menu">
        ${this.getSelectedProfiles()}
        <sl-button href="customer" size="small" outline>
          <sl-icon library="fa" name="fas-calendar"></sl-icon> Künstler finden
        </sl-button>
        ${this.getLoginLogout()}
      </div>
      ${this.getNewArtistProfileMessage()}
      ${this.getNewCustomerProfileMessage()}
    `;
  }

  getSelectedProfiles() {
    if (keycloak.authenticated) {
      return html`
        <span>
          <small>
            <sl-badge variant="primary">
              Ausgewählter Kunde:<b
                >&nbsp;${this.settings.defaultCustomer.name}</b
              ></sl-badge
            >
            <br />
            <sl-badge variant="primary">
              Ausgewählter Künstler:<b
                >&nbsp;${this.settings.defaultArtist.name}</b
              ></sl-badge
            >
          </small>
        </span>
      `;
    }
    return html``;
  }

  getNewCustomerProfileMessage() {
    if (this.noCustomerProfile) {
      return html`
        <div class="content">
          <sl-alert variant="primary" open>
            <sl-icon slot="icon" library="fa" name="fas-info-circle"></sl-icon>
            Kein Kunden/Interessenten-Profil für Sie vorhanden, möchten Sie
            eines erstellen?
            <a href="customer-form">Kunden/Interessenten-Profil erstellen</a>
          </sl-alert>
        </div>
      `;
    }
  }

  getNewArtistProfileMessage() {
    if (this.noArtistProfile) {
      return html`
        <div class="content">
          <sl-alert variant="primary" open>
            <sl-icon slot="icon" library="fa" name="fas-info-circle"></sl-icon>
            Kein Künstler-Profil für Sie vorhanden, möchten Sie eines erstellen?
            <a href="artist-form">Künstlerprofil erstellen</a>
          </sl-alert>
        </div>
      `;
    }
  }

  getLoginLogout() {
    if (keycloak.authenticated) {
      return html`
        <sl-button href="artist-reservation" size="small" outline>
          <sl-icon library="fa" name="fas-book"></sl-icon> Reservationen als
          Künstler einsehen
        </sl-button>
        <sl-button href="artist-selection" size="small" outline>
          <sl-icon library="fa" name="fas-id-badge"></sl-icon> Künstlerprofile
        </sl-button>
        <sl-button href="customer-selection" size="small" outline>
          <sl-icon library="fa" name="fas-building"></sl-icon> Kundenprofile
        </sl-button>
        <sl-button href="customer-reservation" size="small" outline>
          <sl-icon library="fa" name="fas-sticky-note"></sl-icon> Reservationen
          als Kunde einsehen
        </sl-button>
        <sl-button
          @click="${() => keycloak.logout({ redirectUri: `${document.baseURI}/logout-success` })}"
          size="small"
          outline
        >
          <sl-icon library="fa" name="fas-sign-out-alt"></sl-icon> Logout
          (${keycloak.tokenParsed.preferred_username})
        </sl-button>
      `;
    }
    return html`
      <sl-button @click="${() => keycloak.login()}" size="small" outline>
        <sl-icon library="fa" name="fas-sign-in-alt"></sl-icon> Login
      </sl-button>
      <sl-button @click="${() => keycloak.register()}" size="small" outline>
        <sl-icon library="fa" name="fas-user-plus"></sl-icon> Registrieren
      </sl-button>
    `;
  }
}

customElements.define("header-view", HeaderView);
