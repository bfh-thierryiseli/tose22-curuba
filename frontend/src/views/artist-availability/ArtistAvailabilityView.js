import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ReservationService from "../../services/ReservationService";
import UserService from "../../services/UserService";

class ArtistAvailabilityView extends ComponentBase {
  constructor() {
    super();
    this.userService = new UserService();
    this.reservationService = new ReservationService();
  }

  connectedCallback() {
    if (keycloak.authenticated) {
      this.userService
        .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
        .then((settings) => {
          this.artistSelected =
            settings.length > 0 && settings[0].defaultArtist !== null;
          if (this.artistSelected) {
            this.setting = settings[0];
            this.render();
          } else {
            this.render();
          }
        });
    } else {
      window.location.assign("artist-home");
    }
  }

  getTemplate() {
    if (this.artistSelected) {
      return html` <style>
          .availability-form {
            display: flex;
            column-gap: 1rem;
            flex-wrap: wrap;
          }

          .availability-form > * {
            margin-bottom: 1rem;
            width: calc(50% - 0.5rem);
          }
        </style>
        <div class="content">
          <h1>Verfügbarkeit (${this.setting.defaultArtist.name})</h1>
          <p>
            Verfügbarkeiten werden unter den Reservationen mit dem Status
            "verfügbar (AVAILABLE)" aufgeführt.
          </p>
          <form class="availability-form">
            <sl-input
              init
              id="availability-from"
              label="Verfügbarkeit von"
              @sl-change="${(e) => {
                this.startDate = e.target.value;
              }}"
              type="date"
              required
              clearable
            >
            </sl-input>
            <sl-input
              init
              id="availability-to"
              label="Verfügbarkeit bis"
              @sl-change="${(e) => {
                this.endDate = e.target.value;
              }}"
              type="date"
              required
              clearable
            >
            </sl-input>
            <div class="full-width">
              <sl-button
                id="availability-create-submit"
                @click="${(event) => this.submitAvailability(event)}"
                variant="primary"
                >Verfügbarkeit eintragen</sl-button
              >
              <sl-button
                id="availability-delete-submit"
                @click="${(event) => this.submitDeleteAvailability(event)}"
                variant="danger"
                >Verfügbarkeit löschen</sl-button
              >
            </div>
          </form>
        </div>`;
    } else {
      return html` <div class="content">
        <h1>Verfügbarkeit</h1>
        Kein Künstleprofil vorhanden oder ausgewählt.
      </div>`;
    }
  }

  submitAvailability(e) {
    e.preventDefault();
    if (validate(e.target.parentElement.parentElement)) {
      const availability = {
        startDate: dateToISOString(new Date(this.startDate)),
        endDate: dateToISOString(new Date(this.endDate)),
        artist: this.setting.defaultArtist,
      };
      this.reservationService
        .create(availability, "/availability")
        .then((response) => {
          if (response.ok) {
            showAlert(
              "success",
              `Verfügbarkeit von ${new Date(
                this.startDate
              ).toLocaleDateString()} bis ${new Date(
                this.endDate
              ).toLocaleDateString()} erfolgreich eingetragen.`
            );
          }
        });
    }
  }

  submitDeleteAvailability(e) {
    e.preventDefault();
    if (validate(e.target.parentElement.parentElement)) {
      const availability = {
        startDate: dateToISOString(new Date(this.startDate)),
        endDate: dateToISOString(new Date(this.endDate)),
        artist: this.setting.defaultArtist,
      };
      this.reservationService
        .delete("availability", availability)
        .then((response) => {
          if (response.ok) {
            showAlert(
              "success",
              `Verfügbarkeit von ${new Date(
                this.startDate
              ).toLocaleDateString()} bis ${new Date(
                this.endDate
              ).toLocaleDateString()} erfolgreich gelöscht.`
            );
          }
        });
    }
  }
}

customElements.define("artist-availability-view", ArtistAvailabilityView);
