import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ReservationService from "../../services/ReservationService";
import ReservationstatusService from "../../services/ReservationstatusService";
import UserService from "../../services/UserService";
import { Router } from "@vaadin/router";

class CustomerReservationView extends ComponentBase {
  constructor() {
    super();
    this.reservationService = new ReservationService();
    this.userService = new UserService();
    this.reservationstatusService = new ReservationstatusService();
    this.filterReservationstatus = 2;
  }

  connectedCallback() {
    if (keycloak.authenticated) {
      this.userService
        .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
        .then((settings) => {
          this.artistSelected =
            settings.length > 0 && settings[0].defaultCustomer !== undefined;
          if (this.artistSelected) {
            this.setting = settings[0];
            this.reservationstatusService.list().then((reservationstatus) => {
              this.reservationstatus = reservationstatus;
              this.load();
            });
          } else {
            this.render();
          }
        });
    }
  }

  load() {
    this.reservationService
      .list(
        "",
        `?by=customer&id=${this.setting.defaultCustomer.id}&reservationstatus=${this.filterReservationstatus}`,
        false
      )
      .then((reservations) => {
        this.reservations = reservations;
        this.render();
      });
  }

  getTemplate() {
    if (this.artistSelected) {
      return html` <style>
          .reservation-detail > h4 {
            margin-bottom: 0;
            margin-top: 0.5rem;
          }

          .reservation-detail::part(content) {
            padding: 0 1rem 1rem 1rem;
          }
        </style>
        <div class="content">
          <h1>Reservationen (${this.setting.defaultCustomer.name})</h1>
          <div
            style="display: flex;margin-bottom: 1rem;align-items: center;column-gap: 0.5rem;"
          >
            <sl-radio-group
              label="Nach Reservationssatus filtern:"
              fieldset
              value="${this.filterReservationstatus}"
              @sl-change="${(e) => {
                this.reservationstatusChanged(e);
              }}"
            >
              ${this.reservationstatus.map((reservationstatus) =>
                this.getReservationstatus(reservationstatus)
              )}
            </sl-radio-group>
          </div>
          ${this.reservations.map((reservation) =>
            this.getReservation(reservation)
          )}
        </div>`;
    } else {
      return html` <div class="content">
        <h1>Reservationen</h1>
        Kein Kundenprofil vorhanden oder ausgewählt.
      </div>`;
    }
  }

  reservationstatusChanged(e) {
    if (e.target.checked) {
      this.filterReservationstatus = e.target.value;
      this.load();
    }
  }

  getReservationstatus(reservationstatus) {
    return html` <sl-radio-button
      name="option"
      ?checked="${this.filterReservationstatus === reservationstatus.id}"
      value="${reservationstatus.id}"
    >
      ${reservationstatus.name}
    </sl-radio-button>`;
  }

  getReservation(reservation) {
    return html`
      <sl-details class="reservation-detail">
        <div slot="summary">
          ${reservation.artist.name}
          (${new Date(reservation.startAt).toLocaleString()} bis
          ${new Date(reservation.endAt).toLocaleString()})
          ${this.getRatingAction(reservation)}
        </div>
        <h4>Allgemeine Infos</h4>
        <sl-badge variant="${this.getStatusColor(reservation)}"
          >${reservation.status}</sl-badge
        ><br />
        <h4>Veranstaltungsort</h4>
        ${reservation.street}<br />
        ${reservation.postalCode} ${reservation.town}<br />
      </sl-details>
      <br />
    `;
  }

  getRatingAction(reservation) {
    if (reservation.readyForRating) {
      return html` <sl-button
        href="artist-rating-form?artist-id=${reservation.artist
          .id}&reservation-id=${reservation.id}"
        variant="primary"
        size="small"
        ?disabled="${!reservation.readyForRating}"
        outline
        pill
        >Bewerten</sl-button
      >`;
    }
    return html``;
  }

  getStatusColor(reservation) {
    if (reservation.status === "ACCEPTED") {
      return "success";
    } else if (reservation.status === "REJECTED") {
      return "danger";
    }
    return "primary";
  }
}

customElements.define("customer-reservation-view", CustomerReservationView);
