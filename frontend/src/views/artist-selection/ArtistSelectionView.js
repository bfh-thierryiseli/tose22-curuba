import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import UserService from "../../services/UserService";
import TagService from "../../services/TagService";

class ArtistSelectionView extends ComponentBase {
  constructor() {
    super();
    this.userService = new UserService();
    this.tagService = new TagService();
  }

  connectedCallback() {
    this.load();
  }

  load() {
    if (keycloak.authenticated) {
      return this.userService
        .list("", `${keycloak.tokenParsed.preferred_username}/artists`)
        .then((userArtists) => {
          return this.userService
            .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
            .then((settings) => {
              if (settings.length > 0) {
                this.settings = settings[0];
              } else {
                this.settings = {};
                this.settings.defaultArtist = {};
              }
              this.userArtists = userArtists;
              this.render();
            });
        });
    }
  }

  getTemplate() {
    return html`
      <style>
        .list-item > h3 {
          margin-bottom: 0.5rem;
        }

        .tags > h5 {
          margin-bottom: 0.5rem;
        }

        .tags .new-tag {
          display: block;
          width: 100%;
          max-width: 25rem;
          margin-bottom: 0.5rem;
        }

        .tags > sl-tag {
          display: inline-block;
          margin-bottom: 0.5rem;
        }
      </style>
      <div class="content">
        <h1>Künstlerprofile</h1>
        <div style="margin-bottom: 1rem;text-align: right;">
          <sl-button variant="primary" href="artist-form">
            Neues Künstlerprofil
          </sl-button>
        </div>
        <div class="list">
          ${this.userArtists.map((userArtist) =>
            this.getArtistSelection(userArtist.artist)
          )}
        </div>
      </div>
    `;
  }

  getArtistSelection(artist) {
    return html` <sl-card
      class="list-item"
      artist-name="${artist.name.toLowerCase()}"
    >
      <h3>
        ${artist.name}
        <sl-badge variant="neutral" pill
          >${artist.section ? artist.section.name : ""}</sl-badge
        >
      </h3>
      <sl-button
        variant="primary"
        size="small"
        @click="${(e) => {
          this.userService
            .update(
              { defaultArtist: { id: artist.id } },
              `${keycloak.tokenParsed.preferred_username}/settings`
            )
            .then((_) => {
              this.connectedCallback();
              document.querySelector("header-view").connectedCallback();
            });
        }}"
        pill
        .disabled=${this.settings.defaultArtist.id === artist.id}
      >
        ${this.settings.defaultArtist.id === artist.id
          ? "Aktuell ausgewählt"
          : "Auswählen"}
      </sl-button>
      <div class="tags">
        <h5>Stichwörter</h5>
        <form @submit="${(e) => this.addTag(e, artist)}">
          <sl-input
            init
            size="small"
            placeholder="Stichwort hinzufügen"
            help-text="Drücke die Eingabe-/Enter-Taste, um das Stichwort zu bestätigen."
            clearable
            value="${this.newTag}"
            class="new-tag"
            required
            pattern="[a-zA-Z0-9s]*"
            @sl-input="${(event) => this.validateTypedValue(event)}"
          >
          </sl-input>
        </form>
        ${artist.tagArtists.map((tag) => this.getTag(tag))}
      </div>
    </sl-card>`;
  }

  getTag(tagArtist) {
    return html` <sl-tag
      class="tag"
      pill
      removable
      @sl-remove="${(_) => this.clearTag(tagArtist)}"
      >${tagArtist.tag.name}</sl-tag
    >`;
  }

  clearTag(tagArtist) {
    this.tagService.delete(`artist/${tagArtist.id}`).then((_) => {
      this.load();
    });
  }

  validateTypedValue(event) {
    const currentInput = event.target.shadowRoot.querySelector("input");
    if (currentInput.validity.patternMismatch) {
      currentInput.setCustomValidity(
        "Stichwort darf nur Buchstaben und Nummern beinhalten, keine Leerzeichen oder Sonstiges."
      );
    } else {
      currentInput.setCustomValidity("");
    }
  }

  addTag(event, artist) {
    event.preventDefault();
    if (validate(event.target)) {
      const currentSlInput = event.target.querySelector(".new-tag");
      const currentInput = currentSlInput.shadowRoot.querySelector("input");
      this.tagService
        .create({ name: currentInput.value }, `/artist/${artist.id}`)
        .then((_) => {
          this.load().then((_) => {
            currentInput.value = "";
            currentSlInput.value = "";
            currentSlInput.setAttribute("init", "");
          });
        });
    }
  }
}

customElements.define("artist-selection-view", ArtistSelectionView);
