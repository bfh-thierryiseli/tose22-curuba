import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";

class LogoutSuccessView extends ComponentBase {
  constructor() {
    super();
  }

  getTemplate() {
    return html`
      <div class="content">
        <sl-alert variant="success" open>
          <sl-icon slot="icon" library="fa" name="fas-check-circle"></sl-icon>
          <strong>Erfolgreich abgemeldet.</strong><br />
        </sl-alert>
      </div>
    `;
  }
}

customElements.define("logout-success-view", LogoutSuccessView);
