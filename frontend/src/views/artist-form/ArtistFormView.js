import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ArtistService from "../../services/ArtistService";
import SectionService from "../../services/SectionService";

class ArtistFormView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    if (keycloak.authenticated) {
      this.artistService = new ArtistService();
      this.sectionService = new SectionService();
      this.artist = { address: {} };
      this.sectionService.list("", "", true).then((sections) => {
        this.sections = sections;
        this.render();
      });
    }
  }

  getTemplate() {
    return html`
      <style>
        .form {
          display: flex;
          column-gap: 1rem;
          flex-wrap: wrap;
        }

        .form > * {
          margin-bottom: 1rem;
          width: calc(50% - 0.5rem);
        }

        .quarter-width {
          width: calc(25% - 0.75rem);
        }
      </style>
      <div class="content">
        <h1>Künstlerprofil erstellen</h1>
        <form class="form" @submit="${(e) => this.submitArtist(e)}">
          <sl-input
            id="artist-name"
            init
            label="Name"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.artist.name = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-select
            id="artist-section"
            init
            label="Bereich"
            placeholder="Bereich auswählen"
            @sl-change="${(e) => {
              this.artist.section = { id: e.target.value };
            }}"
            required
          >
            <sl-menu>
              ${this.sections.map((section) => this.getSectionItem(section))}
            </sl-menu>
          </sl-select>
          <sl-textarea
            id="artist-description"
            init
            class="full-width"
            label="Beschreibung"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.artist.description = e.target.value;
            }}"
            required
          ></sl-textarea>
          <sl-input
            id="artist-email"
            init
            class="full-width"
            label="Email"
            type="email"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.artist.email = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            id="artist-street"
            init
            class="full-width"
            label="Strasse und Nummer"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.artist.address.street = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            id="artist-postal-code"
            init
            label="Postleitzahl"
            @sl-change="${(e) =>
              (this.artist.address.postalCode = e.target.value)}"
            required
          ></sl-input>
          <sl-input
            id="artist-town"
            init
            label="Ort"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.artist.address.town = e.target.value;
            }}"
            required
          ></sl-input>
          <div class="full-width">
            <sl-button
              id="submit-artist"
              type="submit"
              class="full-width"
              variant="primary"
              >Künstlerprofil erstllen</sl-button
            >
          </div>
        </form>
      </div>
    `;
  }

  getSectionItem(section) {
    return html`<sl-menu-item value="${section.id}"
      >${section.name}</sl-menu-item
    >`;
  }

  removeInit(event) {
    event.target.removeAttribute("init");
  }

  submitArtist(event) {
    event.preventDefault();
    if (validate(event.target)) {
      this.artistService.create(this.artist).then((response) => {
        if (response.ok) {
          location.assign("/artist-selection")
        }
      });
    }
  }
}

customElements.define("artist-form-view", ArtistFormView);
