import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ReservationService from "../../services/ReservationService";

class RservationDetailView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    const queryParams = new URLSearchParams(window.location.search);
    this.reservationService = new ReservationService();
    this.reservationService
      .find(queryParams.get("reservation-id"), true)
      .then((reservation) => {
        this.reservation = reservation;
        this.render();
      });
  }

  getTemplate() {
    return html`
      <div class="content">
        <h1>Reservation (Nr. ${this.reservation.id})</h1>
        <p><strong>Status:</strong> ${this.reservation.status}</p>
        <p>
          <strong>Künstler:</strong>
          <span id="reservation-artist-name"
            >${this.reservation.artist.name}</span
          >
        </p>
        <p>
          <strong>Strasse und Nummer (Veranstaltungsort):</strong> ${this
            .reservation.street}
        </p>
        <p>
          <strong>Postleitzahl (Veranstaltungsort):</strong> ${this.reservation
            .postalCode}
          ${this.reservation.town}
        </p>
        <p>
          <strong>Reservation von:</strong> ${new Date(
            this.reservation.startAt
          ).toLocaleString()}
        </p>
        <p>
          <strong>Reservation bis:</strong> ${new Date(
            this.reservation.endAt
          ).toLocaleString()}
        </p>
      </div>
    `;
  }
}

customElements.define("reservation-detail-view", RservationDetailView);
