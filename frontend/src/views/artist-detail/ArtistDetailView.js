import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ArtistPublicService from "../../services/ArtistPublicService";

class ArtistDetailView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    const queryParams = new URLSearchParams(window.location.search);
    this.artistPublicService = new ArtistPublicService();
    this.artistPublicService
      .find(queryParams.get("artist-id"), true)
      .then((artist) => {
        this.artist = artist;
        this.render();
      });
  }

  getTemplate() {
    return html`
      <div class="content">
        <h1>${this.artist.name}</h1>
      </div>
    `;
  }
}

customElements.define("artist-detail-view", ArtistDetailView);
