import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ArtistPublicService from "../../services/ArtistPublicService";
import RatingPublicService from "../../services/RatingPublicService";
import SectionService from "../../services/SectionService";

class CustomerHomeView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    this.artistService = new ArtistPublicService();
    this.ratingPublicService = new RatingPublicService();
    this.sectionService = new SectionService();
    this.startAt_hour = 20;
    this.startAt_minute = 0;
    this.endAt_hour = 22;
    this.endAt_minute = 0;
    this.filter_nameDescription = "";
    this.filter_section = "";
    this.filter_startAt = "";
    this.filter_endAt = "";
    this.selectedArtist = {};
    this.ratings = [];
    this.loadArtists();
    document.addEventListener("scroll", (e) => this.loadMoreArtist(this));
    document.addEventListener("touchmove", (e) => this.loadMoreArtist(this));
    document.addEventListener("resize", (e) => this.loadMoreArtist(this));
  }

  disconnectedCallback() {
    document.removeEventListener("scroll", (e) => this.loadMoreArtist(this));
    document.removeEventListener("touchmove", (e) => this.loadMoreArtist(this));
    document.removeEventListener("resize", (e) => this.loadMoreArtist(this));
  }

  loadMoreArtist(self) {
    if (
      window.innerHeight + window.scrollY >= document.body.offsetHeight - 10 &&
      self.totalCount > self.artists.length
    ) {
      self.filter_offset += 5;
      self.loadArtists(true);
    }
  }

  loadArtists(loadMore) {
    if (!loadMore) {
      this.filter_max = 5;
      this.filter_offset = 0;
    }
    this.artistService
      .get(
        [
          { key: "nameDescription", value: this.filter_nameDescription },
          { key: "sectionId", value: this.filter_section },
          { key: "startAt", value: this.filter_startAt },
          { key: "endAt", value: this.filter_endAt },
          { key: "max", value: this.filter_max },
          { key: "offset", value: this.filter_offset },
        ],
        "",
        true
      )
      .then((response) => {
        this.totalCount = response.headers.get("X-count");
        response.json().then((artists) => {
          if (loadMore) {
            this.artists = this.artists.concat(artists);
          } else {
            this.artists = artists;
          }
          this.sectionService.list("", "", true).then((sections) => {
            this.sections = sections;
            this.render();
          });
        });
      });
  }

  getTemplate() {
    return html`
      <style>
        sl-rating {
          --symbol-size: 0.8rem;
          margin-left: 0.2rem;
        }

        .filter-section {
          display: flex;
          column-gap: 1rem;
          flex-wrap: wrap;
        }

        .filter-section > * {
          margin-bottom: 1rem;
          width: calc(50% - 0.5rem);
        }

        .quarter-width {
          width: calc(25% - 0.75rem);
        }
      </style>
      <div class="content">
        <h1>Finden Sie den passenden Künstler!</h1>
        <div class="filter-section">
          <sl-input
            id="filter-name-description"
            clearable
            label="Suchkriterium (Name, Beschreibung, Stichwort)"
            @sl-input="${(e) => {
              this.filter_nameDescription = e.target.value;
              this.loadArtists();
            }}"
          ></sl-input>
          <sl-select
            clearable
            label="Bereich"
            placeholder="Bereich auswählen"
            @sl-change="${(e) => {
              this.filter_section = e.target.value;
              this.loadArtists();
            }}"
          >
            <sl-menu>
              ${this.sections.map((section) => this.getSectionItem(section))}
            </sl-menu>
          </sl-select>
          <sl-input
            id="filter-reservation-date"
            label="Reservationsdatum"
            class="full-width"
            @sl-change="${(e) => {
              this.date_at = e.target.value;
              this.setDates();
            }}"
            type="date"
            clearable
          ></sl-input>
          <sl-input
            class="quarter-width"
            label="Stunde"
            value="${this.startAt_hour}"
            min="0"
            max="24"
            @sl-change="${(e) => {
              this.startAt_hour = e.target.value;
              this.setDates();
            }}"
            type="number"
          ></sl-input>
          <sl-input
            class="quarter-width"
            label="Minute"
            value="${this.startAt_minute}"
            min="0"
            max="59"
            @sl-change="${(e) => {
              this.startAt_minute = e.target.value;
              this.setDates();
            }}"
            type="number"
          ></sl-input>
          <sl-input
            class="quarter-width"
            label="Stunde"
            value="${this.endAt_hour}"
            min="0"
            max="24"
            @sl-change="${(e) => {
              this.endAt_hour = e.target.value;
              this.setDates();
            }}"
            type="number"
          ></sl-input>
          <sl-input
            class="quarter-width"
            label="Minute"
            value="${this.endAt_minute}"
            min="0"
            max="59"
            @sl-change="${(e) => {
              this.endAt_minute = e.target.value;
              this.setDates();
            }}"
            type="number"
          ></sl-input>
        </div>
        <sl-tab-group>
          <sl-tab slot="nav" panel="new">Neue Künstler</sl-tab>
          <sl-tab-panel name="new">
            <div class="list">
              ${this.artists.map((artist) => this.getArtist(artist))}
            </div>
          </sl-tab-panel>
        </sl-tab-group>
      </div>
      <sl-dialog
        id="ratings-dialog"
        label="Bewertungen für ${this.selectedArtist.name}"
        class="dialog-width"
        style="--width: 50vw;"
      >
        ${this.ratings.map((rating) => this.getRatingEntry(rating))}
      </sl-dialog>
    `;
  }

  getRatingEntry(rating) {
    return html`
      <div style="display: flex;align-items: center;">
        <b style="font-size: 1rem"> ${rating.name} </b>
        <sl-rating
          precision="0.1"
          value="${rating.rating}"
          readonly
        ></sl-rating>
      </div>
      <sl-button
        id="report-rating"
        class="no-padding"
        size="small"
        variant="text"
        outline
        @click="${(_) => this.reportRating(rating)}"
        >Bewertung als unangemessen melden</sl-button
      >
      <p
        style="border-bottom: 1px solid #AAA; margin-top: 0; margin-bottom: 2rem; padding: 1rem 0;"
      >
        ${rating.text}
      </p>
    `;
  }

  reportRating(rating) {
    this.ratingPublicService.create(rating, "/report", true).then((_) => {
      showAlert(
        "success",
        "Bewertung wurde gemeldet, der Plattformbetreiber wird darüber informiert."
      );
    });
  }

  setDates() {
    if (this.date_at !== "" && this.date_at !== undefined) {
      const startDate = new Date(this.date_at);
      startDate.setHours(parseInt(this.startAt_hour));
      startDate.setMinutes(parseInt(this.startAt_minute));
      startDate.setSeconds(0);
      startDate.setMilliseconds(0);
      this.filter_startAt = encodeURIComponent(dateToISOString(startDate));

      const endDate = new Date(this.date_at);
      endDate.setHours(parseInt(this.endAt_hour));
      endDate.setMinutes(parseInt(this.endAt_minute));
      endDate.setSeconds(0);
      endDate.setMilliseconds(0);
      this.filter_endAt = encodeURIComponent(dateToISOString(endDate));
    } else {
      this.filter_startAt = "";
      this.filter_endAt = "";
    }
    this.loadArtists();
  }

  getSectionItem(section) {
    return html`<sl-menu-item value="${section.id}"
      >${section.name}</sl-menu-item
    >`;
  }

  getArtist(artist) {
    return html`
      <sl-card class="list-item">
        <h3>
          ${artist.name}
          <sl-badge variant="neutral" pill
            >${artist.section ? artist.section.name : ""}</sl-badge
          >
          ${this.getRating(artist)}
        </h3>
        <p>${artist.description}</p>
        <sl-button
          variant="primary"
          outline
          href="reservation-form?artist-id=${artist.id}&start-date=${this
            .filter_startAt}&end-date=${this.filter_endAt}"
          pill
          >Buchen</sl-button
        >
        <sl-button
          variant="primary"
          outline
          href="artist-contact-form?artist-id=${artist.id}&start-date=${this
            .filter_startAt}&end-date=${this.filter_endAt}"
          pill
          >Kontaktieren</sl-button
        >
        ${this.getRatingButton(artist)}
      </sl-card>
    `;
  }

  getRatingButton(artist) {
    if (artist.rating !== undefined) {
      return html`
        <sl-button
          id="ratings"
          rating-artist-name="${artist.name.toLowerCase()}"
          variant="neutral"
          @click="${(_) => this.showRatings(artist)}"
          outline
          pill
          >Zu den Bewertungen</sl-button
        >
      `;
    }
    return html``;
  }

  showRatings(artist) {
    this.selectedArtist = artist;
    this.ratingPublicService
      .list([{ key: "artistId", value: artist.id }], null, true)
      .then((ratings) => {
        this.ratings = ratings;
        this.render();
        this.querySelector("#ratings-dialog").show();
      });
  }

  getRating(artist) {
    if (artist.rating !== undefined) {
      return html`
        <sl-rating
          precision="0.1"
          value="${artist.rating}"
          readonly
        ></sl-rating>
      `;
    }
    return html`<small style="font-size: 0.75rem">Keine Bewertung</small>`;
  }
}

customElements.define("customer-home-view", CustomerHomeView);
