import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import CustomerService from "../../services/CustomerService";

class CustomerDetailView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    const queryParams = new URLSearchParams(window.location.search);
    this.customerService = new CustomerService();
    this.customerService
      .find(queryParams.get("customer-id"))
      .then((customer) => {
        this.customer = customer;
        this.render();
      });
  }

  getTemplate() {
    return html`
      <div class="content">
        <h1>${this.customer.name}</h1>
      </div>
    `;
  }
}

customElements.define("customer-detail-view", CustomerDetailView);
