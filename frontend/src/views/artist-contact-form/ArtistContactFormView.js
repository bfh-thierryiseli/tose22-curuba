import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ArtistPublicService from "../../services/ArtistPublicService";
import ContactArtistService from "../../services/ContactArtistService";
import UserService from "../../services/UserService";
import { Router } from "@vaadin/router";

class ArtistContactFormView extends ComponentBase {
  constructor() {
    super();
    this.contactArtistService = new ContactArtistService();
  }

  connectedCallback() {
    const queryParams = new URLSearchParams(window.location.search);
    this.contactForm = {};
    this.startAt_date = new Date();
    this.startAt_hour = 20;
    this.startAt_minute = 0;
    this.endAt_date = new Date();
    this.endAt_hour = 22;
    this.endAt_minute = 0;
    this.setDates();
    this.userService = new UserService();
    this.artistService = new ArtistPublicService();
    this.artistService
      .find(queryParams.get("artist-id"), true)
      .then((artist) => {
        this.artist = artist;
        if (keycloak.authenticated) {
          this.userService
            .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
            .then((settings) => {
              if (
                settings.length > 0 &&
                settings[0].defaultCustomer !== undefined
              ) {
                this.settings = settings[0];
              } else {
                this.settings = {};
                this.settings.defaultCustomer = {};
                this.settings.defaultCustomer.address = {};
              }
              this.contactForm.name = this.settings.defaultCustomer.name;
              this.contactForm.email = this.settings.defaultCustomer.email;
              this.contactForm.street =
                this.settings.defaultCustomer.address.street;
              this.contactForm.postalCode =
                this.settings.defaultCustomer.address.postalCode;
              this.contactForm.town =
                this.settings.defaultCustomer.address.town;
              this.render();
            });
        } else {
          this.settings = {};
          this.settings.defaultCustomer = {};
          this.settings.defaultCustomer.address = {};
          this.render();
        }
      });
  }

  getTemplate() {
    return html`
      <style>
        .reservation-form {
          display: flex;
          column-gap: 1rem;
          flex-wrap: wrap;
        }

        .reservation-form > * {
          margin-bottom: 1rem;
          width: calc(50% - 0.5rem);
        }

        .quarter-width {
          width: calc(25% - 0.75rem);
        }
      </style>
      <div class="content">
        <h1>Kontaktanfrage an Künstler ${this.artist.name}</h1>
        <form
          class="reservation-form"
          @submit="${(event) => this.submitReservation(event)}"
        >
          <sl-input
            init
            id="contact-name"
            class="full-width"
            value="${this.contactForm.name}"
            label="Name"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.contactForm.name = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            init
            id="contact-email"
            class="full-width"
            value="${this.contactForm.email}"
            label="Email"
            type="email"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.contactForm.email = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            init
            id="contact-street"
            class="full-width"
            value="${this.contactForm.street}"
            label="Strasse und Nummer"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.contactForm.street = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            init
            id="contact-postal-code"
            label="Postleitzahl"
            value="${this.contactForm.postalCode}"
            @sl-change="${(e) =>
              (this.contactForm.postalCode = e.target.value)}"
            required
          ></sl-input>
          <sl-input
            init
            id="contact-town"
            label="Ort"
            value="${this.contactForm.town}"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.contactForm.town = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-textarea
            init
            id="contact-message"
            class="full-width"
            label="Fragen oder Anliegen"
            value="${this.contactForm.message}"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.contactForm.message = e.target.value;
            }}"
            required
          ></sl-textarea>
          <div class="full-width">
            <sl-button
              id="submit-contact"
              type="submit"
              class="full-width"
              variant="primary"
              >Künstler kontaktieren</sl-button
            >
          </div>
        </form>
      </div>
    `;
  }

  removeInit(event) {
    event.target.removeAttribute("init");
  }

  setDates() {
    const startDate = new Date(this.startAt_date);
    startDate.setHours(parseInt(this.startAt_hour));
    startDate.setMinutes(parseInt(this.startAt_minute));
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);
    const endDate = new Date(this.endAt_date);
    endDate.setHours(parseInt(this.endAt_hour));
    endDate.setMinutes(parseInt(this.endAt_minute));
    endDate.setSeconds(0);
    endDate.setMilliseconds(0);
    this.contactForm.startAt = this.toIsoString(startDate);
    this.contactForm.endAt = this.toIsoString(endDate);
  }

  toIsoString(date) {
    var tzo = -date.getTimezoneOffset(),
      dif = tzo >= 0 ? "+" : "-",
      pad = function (num) {
        return (num < 10 ? "0" : "") + num;
      };

    return (
      date.getFullYear() +
      "-" +
      pad(date.getMonth() + 1) +
      "-" +
      pad(date.getDate()) +
      "T" +
      pad(date.getHours()) +
      ":" +
      pad(date.getMinutes()) +
      ":" +
      pad(date.getSeconds()) +
      dif +
      pad(Math.floor(Math.abs(tzo) / 60)) +
      ":" +
      pad(Math.abs(tzo) % 60)
    );
  }

  submitReservation(event) {
    event.preventDefault();
    if (validate(event.target)) {
      this.contactForm.artist = this.artist;
      this.contactArtistService
        .create(this.contactForm, "", true)
        .then((response) => {
          Router.go({
            pathname: "/artist-contact-success",
          });
        });
    }
  }
}

customElements.define("artist-contact-form-view", ArtistContactFormView);
