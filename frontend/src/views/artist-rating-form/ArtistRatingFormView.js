import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ArtistPublicService from "../../services/ArtistPublicService";
import UserService from "../../services/UserService";
import RatingPublicService from "../../services/RatingPublicService";
import ReservationService from "../../services/ReservationService";
import { Router } from "@vaadin/router";

class ArtistRatingFormView extends ComponentBase {
  constructor() {
    super();
    this.artistService = new ArtistPublicService();
    this.userService = new UserService();
    this.ratingPublicService = new RatingPublicService();
    this.reservationService = new ReservationService();
  }

  connectedCallback() {
    const queryParams = new URLSearchParams(window.location.search);
    this.artistService
      .find(queryParams.get("artist-id"), true)
      .then((artist) => {
        this.reservationService
          .find(queryParams.get("reservation-id"), true)
          .then((reservation) => {
            this.artist = artist;
            this.rating = {};
            this.rating.artist = artist;
            this.rating.reservationId = reservation.id;
            this.rating.name = reservation.name;
            this.render();
          });
      });
  }

  getTemplate() {
    return html`
      <style>
        .form {
          display: flex;
          column-gap: 1rem;
          flex-wrap: wrap;
        }

        .form > * {
          margin-bottom: 1rem;
          width: calc(50% - 0.5rem);
        }

        .quarter-width {
          width: calc(25% - 0.75rem);
        }
      </style>
      <div class="content">
        <h1>Künstler ${this.rating.artist.name} bewerten</h1>
        <form class="form" @submit="${(e) => this.submitRating(e)}">
          <sl-textarea
            id="rating-text"
            init
            class="full-width"
            label="Text"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.rating.text = e.target.value;
            }}"
            required
          ></sl-textarea>
          <sl-rating
            id="rating-rating"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.rating.rating = e.target.value;
            }}"
            style="--symbol-size: 2rem;"
            class="full-width"
          ></sl-rating>
          <div class="full-width">
            <sl-button
              id="submit-rating"
              type="submit"
              class="full-width"
              variant="primary"
              >Bewertung abgeben</sl-button
            >
          </div>
        </form>
      </div>
    `;
  }

  removeInit(event) {
    event.target.removeAttribute("init");
  }

  submitRating(event) {
    event.preventDefault();
    if (validate(event.target)) {
      this.ratingPublicService
        .create(this.rating, "", true)
        .then((response) => {
          if (response.ok) {
            response.json().then((_) => {
              showAlert(
                "success",
                `Bewertung für ${this.rating.artist.name} erfolgreich abgegeben.`
              );
              Router.go({
                pathname: "/customer",
              });
            });
          }
        });
    }
  }
}

customElements.define("artist-rating-form-view", ArtistRatingFormView);
