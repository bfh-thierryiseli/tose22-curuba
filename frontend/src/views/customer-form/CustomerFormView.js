import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import CustomerService from "../../services/CustomerService";
import SectionService from "../../services/SectionService";
import { Router } from "@vaadin/router";

class CustomerFormView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    if (keycloak.authenticated) {
      this.customerService = new CustomerService();
      this.sectionService = new SectionService();
      this.customer = { address: {} };
      this.sectionService.list("", "", true).then((sections) => {
        this.sections = sections;
        this.render();
      });
    }
  }

  getTemplate() {
    return html`
      <style>
        .form {
          display: flex;
          column-gap: 1rem;
          flex-wrap: wrap;
        }

        .form > * {
          margin-bottom: 1rem;
          width: calc(50% - 0.5rem);
        }

        .quarter-width {
          width: calc(25% - 0.75rem);
        }
      </style>
      <div class="content">
        <h1>Kundenprofil erstellen</h1>
        <form class="form" @submit="${(e) => this.submitCustomer(e)}">
          <sl-input
            id="customer-name"
            init
            label="Name"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.customer.name = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-select
            id="customer-preference"
            init
            label="Präferenz"
            placeholder="Präferenz auswählen"
            @sl-change="${(e) => {
              this.customer.preference = { id: e.target.value };
            }}"
            required
          >
            <sl-menu>
              ${this.sections.map((section) => this.getSectionItem(section))}
            </sl-menu>
          </sl-select>
          <sl-textarea
            id="customer-description"
            init
            class="full-width"
            label="Beschreibung"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.customer.description = e.target.value;
            }}"
            required
          ></sl-textarea>
          <sl-input
            id="customer-email"
            init
            class="full-width"
            label="Email"
            type="email"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.customer.email = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            id="customer-street"
            init
            class="full-width"
            label="Strasse und Nummer"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.customer.address.street = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            id="customer-postal-code"
            init
            label="Postleitzahl"
            @sl-change="${(e) =>
              (this.customer.address.postalCode = e.target.value)}"
            required
          ></sl-input>
          <sl-input
            id="customer-town"
            init
            label="Ort"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.customer.address.town = e.target.value;
            }}"
            required
          ></sl-input>
          <div class="full-width">
            <sl-button
              id="submit-customer"
              type="submit"
              class="full-width"
              variant="primary"
              >Kundenprofil erstllen</sl-button
            >
          </div>
        </form>
      </div>
    `;
  }

  getSectionItem(section) {
    return html`<sl-menu-item value="${section.id}"
      >${section.name}</sl-menu-item
    >`;
  }

  removeInit(event) {
    event.target.removeAttribute("init");
  }

  submitCustomer(event) {
    event.preventDefault();
    if (validate(event.target)) {
      this.customerService.create(this.customer).then((response) => {
        if (response.ok) {
          location.assign("/customer-selection")
        }
      });
    }
  }
}

customElements.define("customer-form-view", CustomerFormView);
