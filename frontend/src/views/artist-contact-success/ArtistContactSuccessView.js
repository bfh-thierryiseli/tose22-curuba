import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";

class ArtistContactSuccessView extends ComponentBase {
  constructor() {
    super();
  }

  getTemplate() {
    return html`
      <div class="content">
        <sl-alert variant="success" open>
          <sl-icon slot="icon" library="fa" name="fas-check-circle"></sl-icon>
          <strong>Kontaktanfrage wurde erfolgreich versendet.</strong><br />
        </sl-alert>
      </div>
    `;
  }
}

customElements.define("artist-contact-success-view", ArtistContactSuccessView);
