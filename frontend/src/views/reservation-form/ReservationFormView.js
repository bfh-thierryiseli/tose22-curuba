import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import ArtistPublicService from "../../services/ArtistPublicService";
import ReservationService from "../../services/ReservationService";
import UserService from "../../services/UserService";
import { Router } from "@vaadin/router";

class ReservationFormView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    const queryParams = new URLSearchParams(window.location.search);
    this.reservation = {};
    if (
      queryParams.get("start-date") !== "" &&
      queryParams.get("start-date") !== undefined &&
      queryParams.get("end-date") !== "" &&
      queryParams.get("end-date") !== undefined
    ) {
      const startDate = new Date(
        decodeURIComponent(queryParams.get("start-date"))
      );
      const endAt = new Date(decodeURIComponent(queryParams.get("end-date")));
      this.date_at = startDate;
      this.startAt_hour = startDate.getHours();
      this.startAt_minute = startDate.getMinutes();
      this.endAt_hour = endAt.getHours();
      this.endAt_minute = endAt.getMinutes();
    } else {
      this.date_at = new Date();
      this.startAt_hour = 20;
      this.startAt_minute = 0;
      this.endAt_hour = 22;
      this.endAt_minute = 0;
    }
    this.setDates();
    this.userService = new UserService();
    this.artistService = new ArtistPublicService();
    this.reservationService = new ReservationService();
    this.artistService
      .find(queryParams.get("artist-id"), true)
      .then((artist) => {
        this.artist = artist;
        if (keycloak.authenticated) {
          this.userService
            .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
            .then((settings) => {
              if (
                settings.length > 0 &&
                settings[0].defaultCustomer !== undefined
              ) {
                this.settings = settings[0];
              } else {
                this.settings = {};
                this.settings.defaultCustomer = {};
                this.settings.defaultCustomer.address = {};
              }
              this.reservation.name = this.settings.defaultCustomer.name;
              this.reservation.email = this.settings.defaultCustomer.email;
              this.reservation.street =
                this.settings.defaultCustomer.address.street;
              this.reservation.postalCode =
                this.settings.defaultCustomer.address.postalCode;
              this.reservation.town =
                this.settings.defaultCustomer.address.town;
              this.render();
            });
        } else {
          this.settings = {};
          this.settings.defaultCustomer = {};
          this.settings.defaultCustomer.address = {};
          this.render();
        }
      });
  }

  getTemplate() {
    return html`
      <style>
        .reservation-form {
          display: flex;
          column-gap: 1rem;
          flex-wrap: wrap;
        }

        .reservation-form > * {
          margin-bottom: 1rem;
          width: calc(50% - 0.5rem);
        }

        .quarter-width {
          width: calc(25% - 0.75rem);
        }
      </style>
      <div class="content">
        <h1>Künstler ${this.artist.name} buchen</h1>
        <form
          class="reservation-form"
          @submit="${(event) => this.submitReservation(event)}"
        >
          <sl-input
            init
            id="reservation-name"
            class="full-width"
            value="${this.reservation.name}"
            label="Name"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.reservation.name = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            init
            id="reservation-email"
            class="full-width"
            value="${this.reservation.email}"
            label="Email"
            type="email"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.reservation.email = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            init
            id="reservation-street"
            class="full-width"
            value="${this.reservation.street}"
            label="Strasse und Nummer (Veranstaltungsort)"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.reservation.street = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            init
            id="reservation-postalcode"
            label="Postleitzahl (Veranstaltungsort)"
            value="${this.reservation.postalCode}"
            @sl-change="${(e) =>
              (this.reservation.postalCode = e.target.value)}"
            required
          ></sl-input>
          <sl-input
            init
            id="reservation-town"
            label="Ort (Veranstaltungsort)"
            value="${this.reservation.town}"
            @sl-change="${(e) => {
              this.removeInit(e);
              this.reservation.town = e.target.value;
            }}"
            required
          ></sl-input>
          <sl-input
            class="full-width"
            id="reservation-reservationdate"
            label="Reservationdatum"
            value="${this.date_at.toISOString().split("T")[0]}"
            @sl-change="${(e) => {
              this.date_at = e.target.value;
              this.setDates();
            }}"
            type="date"
            required
          ></sl-input>
          <sl-input
            class="quarter-width"
            id="reservation-fromtimehour"
            help-text="Stundenangabe (Start)"
            value="${this.startAt_hour}"
            min="0"
            max="24"
            @sl-change="${(e) => {
              this.startAt_hour = e.target.value;
              this.setDates();
            }}"
            type="number"
            required
          ></sl-input>
          <sl-input
            class="quarter-width"
            id="reservation-fromtimeminute"
            help-text="Minutenangabe (Start)"
            value="${this.startAt_minute}"
            min="0"
            max="59"
            @sl-change="${(e) => {
              this.startAt_minute = e.target.value;
              this.setDates();
            }}"
            type="number"
            required
          ></sl-input>
          <sl-input
            class="quarter-width"
            id="reservation-totimehour"
            help-text="Stundenangabe (Ende)"
            value="${this.endAt_hour}"
            min="0"
            max="24"
            @sl-change="${(e) => {
              this.endAt_hour = e.target.value;
              this.setDates();
            }}"
            type="number"
            required
          ></sl-input>
          <sl-input
            class="quarter-width"
            id="reservation-totimeminute"
            help-text="Minutenangabe (Ende)"
            value="${this.endAt_minute}"
            min="0"
            max="59"
            @sl-change="${(e) => {
              this.endAt_minute = e.target.value;
              this.setDates();
            }}"
            type="number"
            required
          ></sl-input>
          <div class="full-width">
            <sl-button
              id="submit-reservation"
              type="submit"
              class="full-width"
              variant="primary"
              >Künstler anfragen</sl-button
            >
          </div>
        </form>
      </div>
    `;
  }

  removeInit(event) {
    event.target.removeAttribute("init");
  }

  setDates() {
    const startDate = new Date(this.date_at);
    startDate.setHours(parseInt(this.startAt_hour));
    startDate.setMinutes(parseInt(this.startAt_minute));
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);
    const endDate = new Date(this.date_at);
    endDate.setHours(parseInt(this.endAt_hour));
    endDate.setMinutes(parseInt(this.endAt_minute));
    endDate.setSeconds(0);
    endDate.setMilliseconds(0);
    this.reservation.startAt = dateToISOString(startDate);
    this.reservation.endAt = dateToISOString(endDate);
  }

  submitReservation(event) {
    event.preventDefault();
    const inputFields = this.querySelectorAll("sl-input");
    let valid = true;
    inputFields.forEach((input) => {
      input.removeAttribute("init");
      if (input.shadowRoot.querySelector(".input--invalid")) {
        valid = false;
      }
    });
    if (valid) {
      this.reservation.artist = this.artist;
      this.reservation.customer =
        Object.keys(this.settings.defaultCustomer).length > 1
          ? this.settings.defaultCustomer
          : null;
      this.reservationService
        .create(this.reservation, "", true)
        .then((response) => {
          if (response.ok) {
            response.json().then((createdReservation) => {
              Router.go({
                pathname: "/reservation-detail",
                search: `?reservation-id=${createdReservation.id}`,
              });
            });
          }
        });
    }
  }
}

customElements.define("reservation-form-view", ReservationFormView);
