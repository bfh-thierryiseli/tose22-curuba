import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";
import UserService from "../../services/UserService";

class CustomerSelectionView extends ComponentBase {
  constructor() {
    super();
  }

  connectedCallback() {
    if (keycloak.authenticated) {
      this.userService = new UserService();
      this.userService
        .list("", `${keycloak.tokenParsed.preferred_username}/customers`)
        .then((userCustomers) => {
          this.userService
            .list("", `${keycloak.tokenParsed.preferred_username}/settings`)
            .then((settings) => {
              if (
                settings.length > 0 &&
                settings[0].defaultCustomer !== undefined
              ) {
                this.settings = settings[0];
              } else {
                this.settings = {};
                this.settings.defaultCustomer = {};
              }
              this.userCustomers = userCustomers;
              this.render();
            });
        });
    }
  }

  getTemplate() {
    return html`
      <style>
        .list-item > h3 {
          margin-bottom: 0.5rem;
        }
      </style>
      <div class="content">
        <h1>Kundenprofile</h1>
        <div style="margin-bottom: 1rem;text-align: right;">
          <sl-button variant="primary" href="customer-form">
            Neues Kundenprofil
          </sl-button>
        </div>
        <div class="list">
          ${this.userCustomers.map((userCustomer) =>
            this.getArtistSelection(userCustomer.customer)
          )}
        </div>
      </div>
    `;
  }

  getArtistSelection(customer) {
    return html`
      <sl-card class="list-item">
        <h3>
          ${customer.name}
          <sl-badge variant="neutral" pill
            >${customer.preference ? customer.preference.name : ""}</sl-badge
          >
        </h3>
        <sl-button
          variant="primary"
          size="small"
          @click="${(e) => {
            this.userService
              .update(
                { defaultCustomer: { id: customer.id } },
                `${keycloak.tokenParsed.preferred_username}/settings`
              )
              .then((_) => {
                this.connectedCallback();
                document.querySelector("header-view").connectedCallback();
              });
          }}"
          pill
          .disabled=${this.settings.defaultCustomer.id === customer.id}
        >
          ${this.settings.defaultCustomer.id === customer.id
            ? "Aktuell ausgewählt"
            : "Auswählen"}
        </sl-button>
      </sl-card>
    `;
  }
}

customElements.define("customer-selection-view", CustomerSelectionView);
