import { html } from "lit";
import { ComponentBase } from "../../ComponentBase";

class ArtistHomeView extends ComponentBase {
  constructor() {
    super();
  }

  getTemplate() {
    return html`
      <div class="content">
        <h1>Künstler*in mit Leidenschaft?</h1>
        <p>
          Dann registiere dich und lege dein Künstlerprofil an oder logge dich
          ein, wenn du schon ein Konto hast.
        </p>
      </div>
    `;
  }
}

customElements.define("artist-home-view", ArtistHomeView);
