export class ServiceBase {
  constructor(relativeBase) {
    this.backendUrl = "";
    this.relativeBase = relativeBase;
  }

  retrieveConfiguration() {
    return fetch("./config/config.json").then((res) => res.json());
  }

  retrieveBackendUrl() {
    return this.retrieveConfiguration()
      .then((config) => {
        this.backendUrl = `${config.backendUrl}/${this.relativeBase}`;
      })
      .catch((error) => {});
  }

  handleError(promise) {
    return promise
      .then((response) => {
        if (!response.ok) {
          response
            .json()
            .then((errorMessage) => {
              showAlert("danger", errorMessage.message);
            })
            .catch((_) => {
              showAlert(
                "danger",
                "Es gab einen Fehler, bitte vesuche es später nochmals."
              );
            });
        }
        return response;
      })
      .catch((error) => {
        showAlert(
          "danger",
          "Es gab einen Fehler, bitte vesuche es später nochmals."
        );
      });
  }

  async get(queryParam) {
    await this.retrieveBackendUrl();
    console.log("list", this.relativeBase);
    let url = this.backendUrl;
    if (Array.isArray(queryParam)) {
      url +=
        "?" +
        queryParam
          .filter(
            (o) =>
              o["value"] !== null &&
              o["value"] !== undefined &&
              o["value"] !== ""
          )
          .map((o) => o["key"] + "=" + o["value"])
          .join("&");
    } else if (queryParam) {
      console.log("queryParams", queryParam, typeof queryparam == "object");
      url += `?${queryParam.key}=${queryParam.value}`;
    }
    return this.handleError(
      fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${window.keycloak.token}`
        },
      })
    );
  }

  async list(queryParam, path, anonymus) {
    await this.retrieveBackendUrl();
    console.log("list", this.relativeBase);
    let url = this.backendUrl;
    if (path) {
      url += "/" + path;
    }
    if (Array.isArray(queryParam)) {
      url +=
        "?" +
        queryParam
          .filter(
            (o) =>
              o["value"] !== null &&
              o["value"] !== undefined &&
              o["value"] !== ""
          )
          .map((o) => o["key"] + "=" + o["value"])
          .join("&");
    } else if (queryParam) {
      console.log("queryParams", queryParam, typeof queryparam == "object");
      url += `?${queryParam.key}=${queryParam.value}`;
    }
    let headers = {};
    if (anonymus) {
      headers = {
        "Content-Type": "application/json",
      };
    } else {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.keycloak.token}`,
      };
    }
    return this.handleError(
      fetch(url, {
        method: "GET",
        headers: headers,
      })
    )
      .then((result) => result.json())
      .catch((error) => {});
  }

  async create(object, commandSuffix = "", anonymus) {
    console.log("create", this.relativeBase);
    await this.retrieveBackendUrl();
    let headers = {};
    if (anonymus) {
      headers = {
        "Content-Type": "application/json",
      };
    } else {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.keycloak.token}`,
      };
    }
    return this.handleError(
      fetch(this.backendUrl + commandSuffix, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(object),
      })
    );
  }

  async find(id, anonymus) {
    console.log("read", id, this.relativeBase);
    await this.retrieveBackendUrl();
    let headers = {};
    if (anonymus) {
      headers = {
        "Content-Type": "application/json",
      };
    } else {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.keycloak.token}`,
      };
    }
    return this.handleError(
      fetch(`${this.backendUrl}/${id}`, {
        method: "GET",
        headers: headers,
      })
    )
      .then((result) => result.json())
      .catch((error) => {});
  }

  async update(object, serviceName) {
    console.log("update", this.relativeBase);
    await this.retrieveBackendUrl();
    const url = serviceName
      ? this.backendUrl + "/" + serviceName
      : this.backendUrl;
    return this.handleError(
      fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${window.keycloak.token}`,
        },
        body: JSON.stringify(object),
      })
    );
  }

  async delete(id) {
    console.log("delete", this.relativeBase);
    await this.retrieveBackendUrl();
    return this.handleError(
      fetch(`${this.backendUrl}/${id}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${window.keycloak.token}`,
        },
      })
    );
  }

  async delete(id, object) {
    console.log("delete", this.relativeBase);
    await this.retrieveBackendUrl();
    return this.handleError(
      fetch(`${this.backendUrl}/${id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${window.keycloak.token}`,
        },
        body: JSON.stringify(object),
      })
    );
  }
}
