npm ci
npm run build
find ./src -name '*.json' -exec cp --parents \{\} ./build/ \;
mv ./build/src/* ./build/
rm -rf ./build/src