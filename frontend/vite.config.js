export default {
  root: "src",
  base: "",
  build: {
    emptyOutDir: true,
    outDir: "../build",
    target: "es2016",
    sourcemap: true,
    minify: false,
  }
};
