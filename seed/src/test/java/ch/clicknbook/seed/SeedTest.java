package ch.clicknbook.seed;

import org.junit.jupiter.api.Test;

public class SeedTest {
    @Test
    public void seed() {
        DataserviceService dataserviceService = new DataserviceService();
        dataserviceService.cleanup();
        dataserviceService.loadMasterData();
    }
}
