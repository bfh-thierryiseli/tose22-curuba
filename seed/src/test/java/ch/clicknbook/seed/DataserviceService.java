package ch.clicknbook.seed;

import javax.json.Json;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class DataserviceService {
    public static final String DELETE = "deletes";
    public static final String INSERTS = "inserts";
    public static final String LISTS = "lists";
    public static final String INSERTS_MASTERDATA = "inserts/master-data";

    private final ApiService dataserviceDeleteService;
    private final ApiService dataserviceInsertMasterDataService;

    public DataserviceService() {
        WebTarget backendTarget = ClientBuilder.newClient().target(System.getenv("DATASERVICE_URL"));
        dataserviceDeleteService = new ApiService(
                backendTarget,
                DELETE);
        dataserviceInsertMasterDataService = new ApiService(
                backendTarget,
                INSERTS_MASTERDATA);
    }

    public void cleanup() {
        dataserviceDeleteService.delete("");
    }

    public void loadMasterData() {
        dataserviceInsertMasterDataService.post(Json.createObjectBuilder().build());
    }
}
