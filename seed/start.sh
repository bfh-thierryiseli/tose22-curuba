#!/usr/bin/env bash

scriptdir="$(dirname "$0")"
cd "$scriptdir"

BACKEND_URL=http://localhost:8080/clicknbook-backend \
DATASERVICE_URL=http://localhost:8081/clicknbook-dataservice \
mvn clean package