def gitversion = []
def mainProject = "clickNbook"
def namespace = mainProject.toLowerCase()
def appName = "${namespace}-idp"
def env = "dev"
def envUpperCase = env.toUpperCase()

podTemplate(containers: [
        containerTemplate(name: 'helm', image: 'lachlanevenson/k8s-helm:v3.4.2', command: 'cat', ttyEnabled: true),
        containerTemplate(name: 'gitversion', image: 'im5tu/netcore-gitversion:3-alpine', ttyEnabled: true, command: 'cat')]) {
    node(POD_LABEL) {
        stage('Checkout') {
            checkout scm
        }

        container('gitversion') {
            stage('Set version') {
                def output = sh(returnStdout: true, script: "dotnet-gitversion /output json")
                gitversion = readJSON text: output
                currentBuild.displayName = "${gitversion.MajorMinorPatch} (${currentBuild.displayName})"
                echo output
            }
        }

        container('helm') {
            dir("idp") { 
                stage('Deploy to dev') {
                    dir("helm-deploy") {
                        sh "cp ../config/${envUpperCase}.yml ${envUpperCase}.yml"
                        sh "helm repo add bitnami https://charts.bitnami.com/bitnami"
                        sh "helm upgrade -i ${appName} bitnami/keycloak -f ${envUpperCase}.yml -n ${namespace}-${env} --create-namespace --wait"
                    }
                }
            } 
        }
    }
}
