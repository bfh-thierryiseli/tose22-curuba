# tose22-curuba - clicknbook

# Getting Started

## Prerequisites

Check out [Setup instructions](setup/README.md).

## Installing

The easiest way is to run the start.sh:

```bash
./start.sh
```

Wait until all things are ready:

```
Testuser: Username = keycloak, Password = test
Frontend: http://localhost:3000
Backend: http://localhost:8080/clicknbook-backend/
Dataservice: http://localhost:8080/clicknbook-dataservice/
Mailhog: http://localhost:8025
Keycloak: http://localhost:8181/auth
```

If you want to have more separate way, you can run following commands in independent terminals:

```bash
./backend/start.sh
```

```bash
./frontend/start.sh
```

```bash
./dataservice/start.sh
```

# Running Tests

Tests can be started in a new terminal window with:

```bash
./bdd/start.sh
```
