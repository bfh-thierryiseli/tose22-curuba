#!/usr/bin/env bash

scriptdir="$(dirname "$0")"
cd "$scriptdir"

docker-compose down
docker-compose up &
sleep 10
chmod +x ./mvnw
./mvnw clean
./mvnw compile quarkus:dev