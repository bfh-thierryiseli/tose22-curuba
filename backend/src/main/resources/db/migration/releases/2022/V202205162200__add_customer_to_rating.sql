ALTER TABLE rating
    ADD COLUMN customer_id BIGINT;
ALTER TABLE rating
    ADD CONSTRAINT fk_customer_rating
       FOREIGN KEY (customer_id)
           REFERENCES customer (id)