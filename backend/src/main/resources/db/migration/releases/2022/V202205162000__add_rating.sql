create table rating
(
    id          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    artist_id   BIGINT,
    text       VARCHAR(1024),
    rating  SMALLINT,
    CONSTRAINT fk_artist_rating
        FOREIGN KEY (artist_id)
            REFERENCES artist (id)
);