ALTER TABLE rating
    DROP CONSTRAINT fk_customer_rating;

ALTER TABLE rating
    ADD COLUMN name VARCHAR(200);