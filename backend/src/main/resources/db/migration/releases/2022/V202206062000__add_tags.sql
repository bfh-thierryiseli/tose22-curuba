create table tag
(
    id   BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(200)
);

create table tag_artist
(
    id        BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    artist_id BIGINT,
    tag_id BIGINT,
    CONSTRAINT fk_artist_tag_artist
        FOREIGN KEY (artist_id)
            REFERENCES artist (id),
    CONSTRAINT fk_tag_tag_artist
        FOREIGN KEY (tag_id)
            REFERENCES tag (id)
);