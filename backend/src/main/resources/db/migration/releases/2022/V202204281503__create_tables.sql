create table address
(
    id         BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name       VARCHAR(254),
    street     VARCHAR(254),
    postalCode VARCHAR(254),
    town       VARCHAR(254)
);

create table section
(
    id   BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(200)
);

create table artist
(
    id          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name        VARCHAR(200),
    email       VARCHAR(254),
    description VARCHAR(9000),
    rating      SMALLINT,
    section_id  BIGINT,
    address_id  BIGINT,
    CONSTRAINT fk_section_artist
        FOREIGN KEY (section_id)
            REFERENCES section (id),
    CONSTRAINT fk_address_artist
        FOREIGN KEY (address_id)
            REFERENCES address (id)
);

create table customer
(
    id          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name        VARCHAR(200),
    email       VARCHAR(254),
    description VARCHAR(9000),
    section_id  BIGINT,
    address_id  BIGINT,
    CONSTRAINT fk_section_customer
        FOREIGN KEY (section_id)
            REFERENCES section (id),
    CONSTRAINT fk_address_customer
        FOREIGN KEY (address_id)
            REFERENCES address (id)
);

create table reservation
(
    id          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    created_at  TIMESTAMP,
    updated_at  TIMESTAMP,
    start_at    TIMESTAMP,
    end_at      TIMESTAMP,
    status      INT,
    artist_id   BIGINT,
    customer_id BIGINT,
    email       VARCHAR(254),
    name        VARCHAR(254),
    street      VARCHAR(254),
    postalCode  VARCHAR(254),
    town        VARCHAR(254),
    CONSTRAINT fk_artist_reservation
        FOREIGN KEY (artist_id)
            REFERENCES artist (id),
    CONSTRAINT fk_customer_reservation
        FOREIGN KEY (customer_id)
            REFERENCES customer (id)
);

create table user_artist
(
    id        BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    artist_id BIGINT,
    username  VARCHAR(300),
    CONSTRAINT fk_artist_user_artist
        FOREIGN KEY (artist_id)
            REFERENCES artist (id)
);

create table user_customer
(
    id          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    customer_id BIGINT,
    username    VARCHAR(300),
    CONSTRAINT fk_customer_user_customer
        FOREIGN KEY (customer_id)
            REFERENCES customer (id)
);

create table user_setting
(
    id                  BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    default_artist_id   BIGINT,
    default_customer_id BIGINT,
    username            VARCHAR(300),
    CONSTRAINT fk_artist_user_setting
        FOREIGN KEY (default_artist_id)
            REFERENCES artist (id),
    CONSTRAINT fk_customer_user_setting
        FOREIGN KEY (default_customer_id)
            REFERENCES customer (id)
);