ALTER TABLE reservation
    RENAME COLUMN created_at TO createdAt;

ALTER TABLE reservation
    RENAME COLUMN updated_at TO updatedAt;

ALTER TABLE reservation
    RENAME COLUMN start_at TO startAt;

ALTER TABLE reservation
    RENAME COLUMN end_at TO endAt;