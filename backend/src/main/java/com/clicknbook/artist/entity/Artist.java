package com.clicknbook.artist.entity;

import com.clicknbook.address.entity.Address;
import com.clicknbook.rating.entity.Rating;
import com.clicknbook.section.entity.Section;
import com.clicknbook.tag.entity.TagArtist;
import com.clicknbook.user.entity.UserArtist;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.Set;

@NamedQueries({
        @NamedQuery(name = Artist.FIND_BY_ALL, query = "SELECT a FROM Artist a"),
})
@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class Artist {
    public Artist(BigInteger id, String name, String description, Section section, Double rating) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.section = section;
        this.rating = rating;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String email;
    @Column
    private Double rating;
    @ManyToOne
    @JoinColumn(name = "section_id")
    private Section section;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "artist", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("tag")
    private Set<TagArtist> tagArtists;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "artist", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonbTransient
    private Set<UserArtist> userArtists;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "artist", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonbTransient
    private Set<Rating> ratings;

    public static final String FIND_BY_ALL = "Artist.FindAll";
}
