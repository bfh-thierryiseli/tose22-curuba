package com.clicknbook.artist.boundary;

import com.clicknbook.address.entity.Address;
import com.clicknbook.artist.entity.Artist;
import com.clicknbook.section.entity.Section;
import com.clicknbook.user.control.UserControl;
import com.clicknbook.user.entity.UserArtist;
import com.clicknbook.user.entity.UserSetting;
import io.quarkus.security.identity.SecurityIdentity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;

@Path("artists")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ArtistResource {
    @Inject
    EntityManager em;
    @Inject
    UserControl userControl;
    @Inject
    SecurityIdentity securityIdentity;

    @POST
    @Transactional
    public Response create(Artist data) {
        if (data.getId() != null) {
            data.setId(null);
        }

        data.setSection(em.find(Section.class, data.getSection().getId()));

        if (data.getAddress() != null) {
            Address address = em.merge(data.getAddress());
            data.setAddress(address);
        }

        Artist created = em.merge(data);

        String username = securityIdentity.getPrincipal().getName();

        UserArtist userArtist = new UserArtist();
        userArtist.setArtist(created);
        userArtist.setUsername(username);
        em.merge(userArtist);

        UserSetting userSetting = new UserSetting();
        userSetting.setDefaultArtist(created);
        userSetting.setUsername(username);
        userControl.updateSetting(userSetting, username);

        return Response
                .created(null)
                .entity(created)
                .build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam("id") BigInteger id) {
        em.remove(em.find(Artist.class, id));
        return Response
                .ok()
                .build();
    }
}