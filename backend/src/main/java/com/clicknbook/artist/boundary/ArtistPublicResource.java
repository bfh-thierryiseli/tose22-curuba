package com.clicknbook.artist.boundary;

import com.clicknbook.artist.entity.Artist;
import com.clicknbook.reservation.entity.Reservation;
import com.clicknbook.reservation.entity.Reservationstatus;
import com.clicknbook.tag.entity.TagArtist;
import lombok.extern.java.Log;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import javax.ws.rs.Path;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.logging.Level;

@Path("artists/public")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Log
public class ArtistPublicResource {
    @Inject
    EntityManager em;

    @GET
    @Transactional
    public Response get(@QueryParam("nameDescription") String nameDescription,
                        @QueryParam("sectionId") BigInteger sectionId,
                        @QueryParam("startAt") String startAt,
                        @QueryParam("endAt") String endAt,
                        @QueryParam("offset") int offset,
                        @QueryParam("max") int max) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> countQuery;
        Query typedQuery;
        Root root;
        CriteriaQuery criteriaQuery;
        Predicate predicate;
        Class clazz;
        String nameFilter = "%" + (nameDescription != null ? nameDescription.toLowerCase() : "") + "%";
        if (max < 1) {
            max = 5;
        }
        boolean hasDates = startAt != null && !startAt.isEmpty() && endAt != null && !endAt.isEmpty();
        OffsetDateTime start = null;
        OffsetDateTime end = null;
        if (hasDates) {
            try {
                start = OffsetDateTime.parse(startAt);
                end = OffsetDateTime.parse(endAt);
            } catch (Exception e) {
                log.log(Level.FINEST, "Not a valid date...", e);
            }
        }
        if (start != null && end != null) {
            clazz = Reservation.class;
            criteriaQuery = criteriaBuilder.createQuery(Reservation.class);
            root = criteriaQuery.from(Reservation.class);
            root.join("artist").join("section");
            criteriaQuery.select(
                    criteriaBuilder.construct(Artist.class,
                            root.get("artist").get("id"),
                            root.get("artist").get("name"),
                            root.get("artist").get("description"),
                            root.get("artist").get("section"),
                            root.get("artist").get("rating")));
            predicate = criteriaBuilder.and(
                    criteriaBuilder.between(criteriaBuilder.literal(start), root.get("startAt"), root.get("endAt")),
                    criteriaBuilder.between(criteriaBuilder.literal(end), root.get("startAt"), root.get("endAt")),
                    criteriaBuilder.equal(root.get("status"), Reservationstatus.AVAILABLE.getId())
            );
            Subquery<TagArtist> tagArtistSubquery = criteriaQuery.subquery(TagArtist.class);
            Root tagArtistRoot = tagArtistSubquery.from(TagArtist.class);
            tagArtistSubquery.select(tagArtistRoot.get("artist").get("id"));
            tagArtistSubquery.where(criteriaBuilder.like(criteriaBuilder.lower(tagArtistRoot.get("tag").get("name")), nameFilter));
            predicate = criteriaBuilder.and(
                    predicate,
                    criteriaBuilder.or(
                            criteriaBuilder.like(criteriaBuilder.lower(root.get("artist").get("name")), nameFilter),
                            criteriaBuilder.like(criteriaBuilder.lower(root.get("artist").get("description")), nameFilter),
                            root.get("artist").get("id").in(tagArtistSubquery))
            );
            if (sectionId != null) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.equal(root.get("artist").get("section").get("id"), sectionId)
                );
            }
            criteriaQuery.where(predicate);
            criteriaQuery.groupBy(
                    root.get("artist").get("id"),
                    root.get("artist").get("name"),
                    root.get("artist").get("description"),
                    root.get("artist").get("rating"),
                    root.get("artist").get("section").get("id"));
        } else {
            clazz = Artist.class;
            criteriaQuery = criteriaBuilder.createQuery(Artist.class);
            root = criteriaQuery.from(Artist.class);
            criteriaQuery.select(
                    criteriaBuilder.construct(Artist.class,
                            root.get("id"),
                            root.get("name"),
                            root.get("description"),
                            root.get("section"),
                            root.get("rating")));
            Subquery<TagArtist> tagArtistSubquery = criteriaQuery.subquery(TagArtist.class);
            Root tagArtistRoot = tagArtistSubquery.from(TagArtist.class);
            tagArtistSubquery.select(tagArtistRoot.get("artist").get("id"));
            tagArtistSubquery.where(criteriaBuilder.like(criteriaBuilder.lower(tagArtistRoot.get("tag").get("name")), nameFilter));
            predicate = criteriaBuilder.or(
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), nameFilter),
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), nameFilter),
                    root.get("id").in(tagArtistSubquery)
            );
            if (sectionId != null) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.equal(root.get("section").get("id"), sectionId)
                );
            }

            if ((nameDescription != null && !nameDescription.isEmpty()) || sectionId != null) {
                criteriaQuery.where(predicate);
            }
        }

        typedQuery = em.createQuery(criteriaQuery);
        typedQuery.setMaxResults(max);
        typedQuery.setFirstResult(offset);

        countQuery = criteriaBuilder.createQuery(Long.class);
        countQuery.from(clazz);
        countQuery.select(criteriaBuilder.count(root));
        countQuery.where(predicate);

        return Response.ok(typedQuery.getResultList())
                .header("X-count", em.createQuery(countQuery).getSingleResult())
                .header("access-control-expose-headers", "X-count")
                .build();
    }

    @GET
    @Path("{id}")
    @Transactional
    public Artist get(@PathParam("id") BigInteger id) {
        return em.find(Artist.class, id);
    }
}