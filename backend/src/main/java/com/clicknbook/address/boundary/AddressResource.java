package com.clicknbook.address.boundary;

import com.clicknbook.address.entity.Address;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;

@Path("addresses")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AddressResource {
    @Inject
    EntityManager em;

    @POST
    @Transactional
    public Response create(Address data) {
        if (data.getId() != null) {
            data.setId(null);
        }
        Address created = em.merge(data);
        return Response
                .created(null)
                .entity(created)
                .build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam("id") BigInteger id) {
        em.remove(em.find(Address.class, id));
        return Response
                .ok()
                .build();
    }
}