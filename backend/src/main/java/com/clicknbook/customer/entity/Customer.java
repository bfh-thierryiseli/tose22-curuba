package com.clicknbook.customer.entity;

import com.clicknbook.address.entity.Address;
import com.clicknbook.section.entity.Section;
import com.clicknbook.user.entity.UserArtist;
import com.clicknbook.user.entity.UserCustomer;
import com.clicknbook.user.entity.UserSetting;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

@NamedQueries({
        @NamedQuery(name = Customer.FIND_BY_ALL, query = "SELECT c FROM Customer c")
})
@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String email;
    @ManyToOne
    @JoinColumn(name = "section_id")
    private Section preference;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonbTransient
    private Set<UserCustomer> userCustomers;

    public static final String FIND_BY_ALL = "Customer.FindAll";
}
