package com.clicknbook.customer.boundary;

import com.clicknbook.address.entity.Address;
import com.clicknbook.artist.entity.Artist;
import com.clicknbook.customer.entity.Customer;
import com.clicknbook.section.entity.Section;
import com.clicknbook.user.control.UserControl;
import com.clicknbook.user.entity.UserCustomer;
import com.clicknbook.user.entity.UserSetting;
import io.quarkus.security.identity.SecurityIdentity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.List;

@Path("customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {
    @Inject
    EntityManager em;
    @Inject
    UserControl userControl;
    @Inject
    SecurityIdentity securityIdentity;

    @GET
    @Transactional
    public List<Customer> get() {
        return em.createNamedQuery(Customer.FIND_BY_ALL, Customer.class)
                .getResultList();
    }

    @POST
    @Transactional
    public Response create(Customer data) {
        if (data.getId() != null) {
            data.setId(null);
        }

        data.setPreference(em.find(Section.class, data.getPreference().getId()));

        if (data.getAddress() != null) {
            Address address = em.merge(data.getAddress());
            data.setAddress(address);
        }

        Customer created = em.merge(data);

        String username = securityIdentity.getPrincipal().getName();

        UserCustomer userCustomer = new UserCustomer();
        userCustomer.setCustomer(created);
        userCustomer.setUsername(username);
        em.merge(userCustomer);

        UserSetting userSetting = new UserSetting();
        userSetting.setDefaultCustomer(created);
        userSetting.setUsername(username);
        userControl.updateSetting(userSetting, username);

        return Response
                .created(null)
                .entity(created)
                .build();
    }

    @GET
    @Path("{id}")
    @Transactional
    public Customer get(@PathParam("id") BigInteger id) {
        return em.find(Customer.class, id);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam("id") BigInteger id) {
        em.remove(em.find(Customer.class, id));
        return Response
                .ok()
                .build();
    }
}