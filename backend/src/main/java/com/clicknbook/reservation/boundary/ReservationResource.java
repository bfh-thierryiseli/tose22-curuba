package com.clicknbook.reservation.boundary;

import com.clicknbook.ApplicationException;
import com.clicknbook.reservation.entity.Reservation;
import com.clicknbook.reservation.entity.ReservationAvailability;
import com.clicknbook.reservation.entity.Reservationstatus;
import com.clicknbook.reservation.entity.ReservationstatusDto;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Path("reservations")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ReservationResource {
    @Inject
    EntityManager em;

    @GET
    @Transactional
    public List<Reservation> get(@QueryParam("by") String by, @QueryParam("id") BigInteger id, @QueryParam("reservationstatus") Integer reservationstatus) {
        if (by != null && by.equals("artist")) {
            return em
                    .createNamedQuery(Reservation.FIND_BY_ARTIST_STATUS, Reservation.class)
                    .setParameter("id", id)
                    .setParameter("reservationstatus", Reservationstatus.fromId(reservationstatus))
                    .getResultList();
        } else if (by != null && by.equals("customer")) {
            return em
                    .createNamedQuery(Reservation.FIND_BY_CUSTOMER_STATUS, Reservation.class)
                    .setParameter("id", id)
                    .setParameter("reservationstatus", Reservationstatus.fromId(reservationstatus))
                    .getResultList();
        }
        return em.createNamedQuery(Reservation.FIND_BY_ALL, Reservation.class).getResultList();
    }

    @GET
    @Path("reservationstatus")
    public List<ReservationstatusDto> getReservationstatus() {
        return Arrays.stream(Reservationstatus.values())
                .map(x -> new ReservationstatusDto(x.getId(), x.name()))
                .collect(Collectors.toList());
    }

    @GET
    @Path("{id}")
    @Transactional
    public Reservation get(@PathParam("id") BigInteger id) {
        return em.find(Reservation.class, id);
    }

    @DELETE
    @Path("availability")
    @Transactional
    public Response deleteAilability(ReservationAvailability reservationAvailability) {
        if (reservationAvailability.getStartDate().isBefore(OffsetDateTime.now())) {
            throw new ApplicationException("Verfügbarkeitsänderungen müssen in der Zukunft liegen.");
        }
        if (reservationAvailability.getStartDate().isAfter(reservationAvailability.getEndDate())) {
            throw new ApplicationException("\"Verfügbarkeit von\"-Datum muss vor dem \"Verfügbakeit bis\"-Datum liegen.");
        }
        OffsetDateTime startDate = reservationAvailability
                .getStartDate()
                .withSecond(0)
                .withMinute(0)
                .withHour(0);
        OffsetDateTime endDate = reservationAvailability
                .getEndDate()
                .withSecond(0)
                .withMinute(0)
                .withHour(0);
        List<Reservation> reservations = em.createNamedQuery(Reservation.FIND_BY_ARTIST_DATE, Reservation.class)
                .setParameter("id", reservationAvailability.getArtist().getId())
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
        reservations
                .stream()
                .filter((x -> !x.getStatus().equals(Reservationstatus.ACCEPTED) &&
                        !x.getStatus().equals(Reservationstatus.REQUESTED)))
                .forEach(r -> em.remove(r));
        return Response.ok().build();
    }

    @POST
    @Path("availability")
    @Transactional
    public Response createAvailability(ReservationAvailability reservationAvailability) {
        if (reservationAvailability.getStartDate().isBefore(OffsetDateTime.now())) {
            throw new ApplicationException("Verfügbarkeitsänderungen müssen in der Zukunft liegen.");
        }
        if (reservationAvailability.getStartDate().isAfter(reservationAvailability.getEndDate())) {
            throw new ApplicationException("\"Verfügbarkeit von\"-Datum muss vor dem \"Verfügbakeit bis\"-Datum liegen.");
        }
        OffsetDateTime startDate = reservationAvailability
                .getStartDate()
                .withSecond(0)
                .withMinute(0)
                .withHour(0);
        OffsetDateTime endDate = reservationAvailability
                .getEndDate()
                .withSecond(0)
                .withMinute(0)
                .withHour(0)
                .plusDays(1);
        List<Reservation> reservations = em.createNamedQuery(Reservation.FIND_BY_ARTIST_DATE, Reservation.class)
                .setParameter("id", reservationAvailability.getArtist().getId())
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
        List<OffsetDateTime> datesToIgnore = new ArrayList<>();
        reservations
                .forEach(r -> {
                    if (!r.getStatus().equals(Reservationstatus.ACCEPTED) &&
                            !r.getStatus().equals(Reservationstatus.REQUESTED)) {
                        em.remove(r);
                    } else {
                        datesToIgnore.add(r.getStartAt());
                    }
                });
        OffsetDateTime currentEndDate;
        while (startDate.isBefore(endDate)) {
            Reservation reservation = new Reservation();
            LocalDate date = startDate.toLocalDate();
            currentEndDate = startDate.plusDays(1);
            if (datesToIgnore.stream().anyMatch(x -> x.toLocalDate().equals(date))) {
                startDate = currentEndDate;
                continue;
            }
            reservation.setStartAt(startDate);
            reservation.setEndAt(currentEndDate);
            reservation.setArtist(reservationAvailability.getArtist());
            reservation.setStatus(Reservationstatus.AVAILABLE);
            startDate = currentEndDate;
            em.merge(reservation);
        }
        return Response.ok().build();
    }

    @POST
    @Transactional
    public Response create(Reservation data) {
        if (data.getId() != null) {
            data.setId(null);
        }
        if (data.getStartAt().minusDays(1).isBefore(OffsetDateTime.now())) {
            throw new ApplicationException("Eine Reservation kann spätestens 24 Stunden vor dem Event gebucht werden und muss in der Zukunft liegen.");
        }
        if (data.getStartAt().isAfter(data.getEndAt())) {
            throw new ApplicationException("Reservationszeit \"Start\" muss vor der Reservationszeit \"Ende\" liegen.");
        }
        List<Reservation> reservations = em.createNamedQuery(Reservation.FIND_BY_ARTIST_DATE, Reservation.class)
                .setParameter("id", data.getArtist().getId())
                .setParameter("startDate", data.getStartAt())
                .setParameter("endDate", data.getEndAt())
                .getResultList();
        if (reservations.size() == 1 && reservations.get(0).getStatus().equals(Reservationstatus.AVAILABLE)) {
            reservations.forEach(r -> em.remove(r));
            data.setStatus(Reservationstatus.ACCEPTED);
        } else {
            data.setStatus(Reservationstatus.REQUESTED);
        }
        data.setCreatedAt(OffsetDateTime.now());
        Reservation created = em.merge(data);
        return Response
                .created(null)
                .entity(created)
                .build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam("id") BigInteger id) {
        em.remove(em.find(Reservation.class, id));
        return Response
                .ok()
                .build();
    }
}
