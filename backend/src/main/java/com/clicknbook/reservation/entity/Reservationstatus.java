package com.clicknbook.reservation.entity;

import java.util.Arrays;

public enum Reservationstatus {
    AVAILABLE(0),
    REQUESTED(1),
    ACCEPTED(2),
    REJECTED(3);

    final int id;

    Reservationstatus(int i) {
        this.id = i;
    }

    public static Reservationstatus fromId(int id, Reservationstatus status) {
        return Arrays.stream(Reservationstatus.values()).filter(s -> s.getId().equals(id)).findFirst().orElse(status);
    }

    public static Reservationstatus fromId(int id) {
        return fromId(id, null);
    }

    public static Reservationstatus fromId(String id) {
        return fromId(Integer.parseInt(id), null);
    }

    public Integer getId() {
        return id;
    }

}
