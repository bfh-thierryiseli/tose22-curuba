package com.clicknbook.reservation.entity;

import com.clicknbook.artist.entity.Artist;
import com.clicknbook.customer.entity.Customer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.OffsetDateTime;

@NamedQueries({
        @NamedQuery(name = Reservation.FIND_BY_ALL, query = "SELECT r FROM Reservation r"),
        @NamedQuery(name = Reservation.FIND_BY_ARTIST_STATUS, query = "SELECT r FROM Reservation r WHERE artist.id = :id AND status = :reservationstatus ORDER BY startAt DESC"),
        @NamedQuery(name = Reservation.FIND_BY_CUSTOMER_STATUS, query = "SELECT r FROM Reservation r WHERE customer.id = :id AND status = :reservationstatus ORDER BY startAt DESC"),
        @NamedQuery(name = Reservation.FIND_BY_ARTIST_DATE, query = "SELECT r FROM Reservation r WHERE artist.id = :id AND r.startAt <= :startDate AND r.endAt >= :endDate"),
        @NamedQuery(name = Reservation.FIND_BY_END_DATE_STATUS_NOT_RATED_NOTIFIED, query = "SELECT r FROM Reservation r WHERE r.endAt < :endDate AND r.rated is null AND r.status = :reservationstatus AND r.ratingNotified is null")
})
@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @Column
    private OffsetDateTime createdAt;
    @Column
    private OffsetDateTime updatedAt;
    @Column
    private OffsetDateTime startAt;
    @Column
    private OffsetDateTime endAt;
    @Column
    private String name;
    @Column
    private String email;
    @Column
    private String street;
    @Column
    private String postalCode;
    @Column
    private String town;
    @Column
    private Boolean ratingNotified;
    @Column
    private Boolean rated;
    @ManyToOne
    @JoinColumn(name = "artist_id")
    private Artist artist;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @Convert(converter = ReservationstatusConverter.class)
    private Reservationstatus status;

    public boolean getReadyForRating() {
        return (rated == null || !rated) && endAt.isBefore(OffsetDateTime.now()) && Reservationstatus.ACCEPTED.equals(status);
    }

    public static final String FIND_BY_ALL = "Reservation.FindAll";
    public static final String FIND_BY_ARTIST_STATUS = "Reservation.FindByArtistReservationStatus";
    public static final String FIND_BY_CUSTOMER_STATUS = "Reservation.FindByCustomerReservationStatus";
    public static final String FIND_BY_ARTIST_DATE = "Reservation.FindByArtistDate";
    public static final String FIND_BY_END_DATE_STATUS_NOT_RATED_NOTIFIED = "Reservation.FindByAEndDateStatusNotRatedNotified";
}