package com.clicknbook.reservation.entity;

import javax.persistence.AttributeConverter;

public class ReservationstatusConverter implements AttributeConverter<Reservationstatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Reservationstatus status) {
        if (status == null) {
            return null;
        }
        return status.getId();
    }

    @Override
    public Reservationstatus convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return Reservationstatus.fromId(value);
    }
}

