package com.clicknbook.reservation.entity;

import com.clicknbook.artist.entity.Artist;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@NoArgsConstructor
@Setter
@Getter
public class ReservationAvailability {
    private OffsetDateTime startDate;
    private OffsetDateTime endDate;
    private Artist artist;
}
