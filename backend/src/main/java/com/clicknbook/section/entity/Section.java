package com.clicknbook.section.entity;

import com.clicknbook.artist.entity.Artist;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = Section.FIND_BY_ALL, query = "SELECT s FROM Section s")
})
@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class Section {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @Column
    private String name;
    @OneToMany(mappedBy = "section", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonbTransient
    private List<Artist> artists;

    public static final String FIND_BY_ALL = "Section.FindAll";
}
