package com.clicknbook.section.boundary;

import com.clicknbook.customer.entity.Customer;
import com.clicknbook.section.entity.Section;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.List;

@Path("sections")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SectionResource {
    @Inject
    EntityManager em;

    @GET
    @Transactional
    public List<Section> get() {
        return em.createNamedQuery(Section.FIND_BY_ALL, Section.class)
                .getResultList();
    }

    @POST
    @Transactional
    public Response create(Section data) {
        if (data.getId() != null) {
            data.setId(null);
        }
        Section created = em.merge(data);
        return Response
                .created(null)
                .entity(created)
                .build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam("id") BigInteger id) {
        em.remove(em.find(Section.class, id));
        return Response
                .ok()
                .build();
    }
}