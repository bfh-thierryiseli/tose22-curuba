package com.clicknbook.rating.entity;

import com.clicknbook.artist.entity.Artist;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
@NamedQueries({
        @NamedQuery(name = Rating.FIND_BY_ARTIST, query = "SELECT r FROM Rating r WHERE r.artist.id = :artistId"),
})
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @Column
    private String text;
    @Column
    private Short rating;
    @ManyToOne
    @JoinColumn(name = "artist_id")
    private Artist artist;
    @Column
    private String name;
    @Transient
    private BigInteger reservationId;

    public static final String FIND_BY_ARTIST = "Rating.FindByArtist";
}
