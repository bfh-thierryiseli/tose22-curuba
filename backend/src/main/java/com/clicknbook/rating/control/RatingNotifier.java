package com.clicknbook.rating.control;

import com.clicknbook.reservation.entity.Reservation;
import com.clicknbook.reservation.entity.Reservationstatus;
import io.quarkus.scheduler.Scheduled;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Properties;

@ApplicationScoped
public class RatingNotifier {
    @ConfigProperty(name = "mail.host")
    String MAIL_HOST;
    @ConfigProperty(name = "mail.port")
    String MAIL_PORT;
    @ConfigProperty(name = "mail.sender")
    String MAIL_SENDER;
    @ConfigProperty(name = "frontend.url")
    String FRONTEND_URL;
    @Inject
    EntityManager em;

    @Scheduled(every = "1m")
    @Transactional
    void notifyRating() throws MessagingException {
        List<Reservation> reservations =
                em.createNamedQuery(Reservation.FIND_BY_END_DATE_STATUS_NOT_RATED_NOTIFIED, Reservation.class)
                        .setParameter("endDate", OffsetDateTime.now().plusHours(2))
                        .setParameter("reservationstatus", Reservationstatus.ACCEPTED)
                        .getResultList();
        for (Reservation r : reservations) {
            Message message = getMessage();
            message.setFrom(new InternetAddress(MAIL_SENDER));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(r.getEmail()));
            message.setSubject("Erinnerung: Künstler bewerten!");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Sie haben in naher Vergangenheit den Künstler ");
            stringBuilder.append(r.getArtist().getName());
            stringBuilder.append(" gebucht.");
            stringBuilder.append("<br />");
            stringBuilder.append("<br />");
            stringBuilder.append("Nun können Sie diesen mit folgendem Link bewerten: ");
            stringBuilder.append("<a href=\"" + FRONTEND_URL + "/artist-rating-form?artist-id=" + r.getArtist().getId() + "&reservation-id=" + r.getId() + "\">");
            stringBuilder.append(FRONTEND_URL + "/artist-rating-form?artist-id=" + r.getArtist().getId() + "&reservation-id=" + r.getId());
            stringBuilder.append("</a>");
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(stringBuilder.toString(), "text/html; charset=utf-8");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            message.setContent(multipart);
            Transport.send(message);
            r.setRatingNotified(true);
            em.merge(r);
        }
    }

    private Message getMessage() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", MAIL_HOST);
        properties.put("mail.smtp.port", MAIL_PORT);
        properties.put("mail.mime.charset", "utf-8");
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("", "");
            }
        });
        return new MimeMessage(session);
    }
}
