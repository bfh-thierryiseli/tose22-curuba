package com.clicknbook.rating.boundary;

import com.clicknbook.artist.entity.Artist;
import com.clicknbook.rating.entity.Rating;
import com.clicknbook.reservation.entity.Reservation;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.flywaydb.core.internal.database.oracle.OracleTable;

import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.List;
import java.util.Properties;

@Path("ratings/public")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RatingPublicResource {
    @ConfigProperty(name = "mail.host")
    String MAIL_HOST;
    @ConfigProperty(name = "mail.port")
    String MAIL_PORT;
    @ConfigProperty(name = "mail.sender")
    String MAIL_SENDER;
    @ConfigProperty(name = "admin.mail")
    String ADMIN_MAIL;

    @Inject
    EntityManager em;

    @GET
    @Transactional
    public List<Rating> get(@QueryParam("artistId") BigInteger artistId) {
        List<Rating> ratings = em.createNamedQuery(Rating.FIND_BY_ARTIST, Rating.class)
                .setParameter("artistId", artistId)
                .getResultList();
        return ratings;
    }

    @POST
    @Path("report")
    @Transactional
    public Response report(Rating rating) throws MessagingException {
        rating = em.find(Rating.class, rating.getId());
        Message message = getMessage();
        message.setFrom(new InternetAddress(MAIL_SENDER));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(ADMIN_MAIL));
        message.setSubject("Unangemessene Bewertung!");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<b>ID:</b>");
        stringBuilder.append("<br />");
        stringBuilder.append(rating.getId());
        stringBuilder.append("<br />");
        stringBuilder.append("<br />");
        stringBuilder.append("<b>Bewertung:</b>");
        stringBuilder.append("<br />");
        stringBuilder.append(rating.getText());
        stringBuilder.append("<br />");
        stringBuilder.append("<br />");
        stringBuilder.append("<b>Knünstler:</b>");
        stringBuilder.append("<br />");
        stringBuilder.append(rating.getArtist().getName());
        stringBuilder.append("<br />");
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(stringBuilder.toString(), "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        message.setContent(multipart);
        Transport.send(message);
        return Response.ok().build();
    }

    @POST
    @Transactional
    public Response create(Rating data) {
        if (data.getId() != null) {
            data.setId(null);
        }
        Rating created = em.merge(data);
        Reservation reservation = em.find(Reservation.class, data.getReservationId());
        reservation.setRated(true);
        TypedQuery<Double> query = em.createQuery("SELECT coalesce(AVG(r.rating), 0) from Rating r where r.artist.id = :artistId group by r.artist.id", Double.class);
        query.setParameter("artistId", data.getArtist().getId());
        double ratingAverage = query.getSingleResult();
        Artist artist = em.find(Artist.class, data.getArtist().getId());
        artist.setRating(ratingAverage);
        return Response
                .created(null)
                .entity(created)
                .build();
    }

    private Message getMessage() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", MAIL_HOST);
        properties.put("mail.smtp.port", MAIL_PORT);
        properties.put("mail.mime.charset", "utf-8");
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("", "");
            }
        });
        return new MimeMessage(session);
    }
}