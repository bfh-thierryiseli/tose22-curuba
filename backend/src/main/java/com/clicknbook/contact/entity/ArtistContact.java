package com.clicknbook.contact.entity;

import com.clicknbook.artist.entity.Artist;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
public class ArtistContact {
    private String name;
    private String email;
    private String street;
    private String postalCode;
    private String town;
    private String message;
    private Artist artist;
}
