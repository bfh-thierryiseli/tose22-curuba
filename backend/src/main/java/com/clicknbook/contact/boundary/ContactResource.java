package com.clicknbook.contact.boundary;

import com.clicknbook.contact.entity.ArtistContact;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Properties;

@Path("contact")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ContactResource {
    @ConfigProperty(name = "mail.host")
    String MAIL_HOST;
    @ConfigProperty(name = "mail.port")
    String MAIL_PORT;
    @ConfigProperty(name = "mail.sender")
    String MAIL_SENDER;

    @POST
    @Path("artist")
    public Response sendArtistContact(ArtistContact artistContact) throws MessagingException {
        Message message = getMessage();
        message.setFrom(new InternetAddress(MAIL_SENDER));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(artistContact.getArtist().getEmail()));
        message.setSubject("Neue Kontaktanfrage von: "  + artistContact.getName());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<b>Nachricht:</b>");
        stringBuilder.append("<br />");
        stringBuilder.append(artistContact.getMessage());
        stringBuilder.append("<br />");
        stringBuilder.append("<br />");
        stringBuilder.append("<b>Kontakt:</b>");
        stringBuilder.append("<br />");
        stringBuilder.append(artistContact.getName());
        stringBuilder.append("<br />");
        stringBuilder.append(artistContact.getEmail());
        stringBuilder.append("<br />");
        stringBuilder.append(artistContact.getStreet());
        stringBuilder.append("<br />");
        stringBuilder.append(artistContact.getPostalCode() + " " + artistContact.getTown());
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(stringBuilder.toString(), "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        message.setContent(multipart);
        Transport.send(message);
        return Response.ok().build();
    }

    private Message getMessage() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", MAIL_HOST);
        properties.put("mail.smtp.port", MAIL_PORT);
        properties.put("mail.mime.charset", "utf-8");
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("", "");
            }
        });
        return new MimeMessage(session);
    }
}
