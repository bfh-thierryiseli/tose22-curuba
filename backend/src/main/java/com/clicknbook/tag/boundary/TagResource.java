package com.clicknbook.tag.boundary;

import com.clicknbook.artist.entity.Artist;
import com.clicknbook.tag.entity.Tag;
import com.clicknbook.tag.entity.TagArtist;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.List;

@Path("tags")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TagResource {
    @Inject
    EntityManager em;

    @GET
    @Path("artist/{artistId}")
    @Transactional
    public List<TagArtist> getArtistTags(@PathParam("artistId") BigInteger artistId) {
        return em
                .createNamedQuery(TagArtist.FIND_BY_TAG_ID, TagArtist.class)
                .setParameter("artistId", artistId)
                .getResultList();
    }

    @DELETE
    @Path("artist/{tagArtistId}")
    @Transactional
    public Response deleteArtistTags(@PathParam("tagArtistId") BigInteger tagArtistId) {
        em.createNamedQuery(TagArtist.DELETE_BY_ID)
                .setParameter("id", tagArtistId)
                .executeUpdate();
        return Response.ok().build();
    }

    @POST
    @Path("artist/{artistId}")
    @Transactional
    public Response postArtistTags(@PathParam("artistId") BigInteger artistId, Tag data) {
        Tag tag = ensureTag(data);
        Artist artist = em.find(Artist.class, artistId);
        TagArtist tagArtist = new TagArtist();
        tagArtist.setTag(tag);
        tagArtist.setArtist(artist);
        List<TagArtist> tagArtists = em
                .createNamedQuery(TagArtist.FIND_BY_TAG_ID_ARTIST_ID, TagArtist.class)
                .setParameter("artistId", artistId)
                .setParameter("tagId", tag.getId())
                .getResultList();
        if (tagArtists.size() < 1) {
            tagArtist = em.merge(tagArtist);
        }
        return Response.ok().entity(tagArtist).build();
    }

    private Tag ensureTag(Tag tag) {
        List<Tag> tags = em
                .createNamedQuery(Tag.FIND_BY_NAME, Tag.class)
                .setParameter("name", tag.getName())
                .getResultList();
        if (tags.size() > 0) {
            return tags.get(0);
        }
        return em.merge(tag);
    }
}