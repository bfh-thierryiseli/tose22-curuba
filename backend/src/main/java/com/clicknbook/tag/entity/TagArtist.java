package com.clicknbook.tag.entity;

import com.clicknbook.artist.entity.Artist;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.math.BigInteger;

@NamedQueries({
        @NamedQuery(name = TagArtist.FIND_BY_TAG_ID, query = "SELECT ta FROM TagArtist ta WHERE ta.tag.id = :tagId"),
        @NamedQuery(name = TagArtist.FIND_BY_TAG_ID_ARTIST_ID, query = "SELECT ta FROM TagArtist ta WHERE ta.tag.id = :tagId AND ta.artist.id = :artistId"),
        @NamedQuery(name = TagArtist.DELETE_BY_ID, query = "DELETE FROM TagArtist ta WHERE ta.id = :id")
})
@Entity
@Table(name = "tag_artist")
@NoArgsConstructor
@Setter
@Getter
public class TagArtist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @ManyToOne
    @JoinColumn(name = "artist_id")
    @JsonbTransient
    private Artist artist;
    @ManyToOne
    @JoinColumn(name = "tag_id")
    private Tag tag;

    public static final String FIND_BY_TAG_ID = "TagArtist.FindByTagId";
    public static final String FIND_BY_TAG_ID_ARTIST_ID = "TagArtist.FindByTagIdArtistId";
    public static final String DELETE_BY_ID = "TagArtist.DeleteById";
}
