package com.clicknbook.tag.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.Set;

@NamedQueries({
        @NamedQuery(name = Tag.FIND_BY_NAME, query = "SELECT t FROM Tag t WHERE t.name = :name")
})
@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @Column
    private String name;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tag")
    @JsonbTransient
    private Set<TagArtist> tagArtists;
    public static final String FIND_BY_NAME = "Tag.FindByName";
}
