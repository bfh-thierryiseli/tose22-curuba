package com.clicknbook.user.entity;

import com.clicknbook.customer.entity.Customer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@NamedQueries({
        @NamedQuery(name = UserCustomer.FIND_BY_USER_USERNAME, query = "SELECT uc FROM UserCustomer uc WHERE uc.username = :username")
})
@Entity
@Table(name = "user_customer")
@NoArgsConstructor
@Setter
@Getter
public class UserCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @Column
    private String username;

    public static final String FIND_BY_USER_USERNAME = "UserCustomer.FindByUsername";
}
