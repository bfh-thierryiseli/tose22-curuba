package com.clicknbook.user.entity;

import com.clicknbook.artist.entity.Artist;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@NamedQueries({
        @NamedQuery(name = UserArtist.FIND_BY_USER_USERNAME, query = "SELECT ua FROM UserArtist ua WHERE ua.username = :username")
})
@Entity
@Table(name = "user_artist")
@NoArgsConstructor
@Setter
@Getter
public class UserArtist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @ManyToOne
    @JoinColumn(name = "artist_id")
    private Artist artist;
    @Column
    private String username;

    public static final String FIND_BY_USER_USERNAME = "UserArtist.FindByUsername";
}
