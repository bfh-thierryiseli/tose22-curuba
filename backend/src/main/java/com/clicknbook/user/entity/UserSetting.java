package com.clicknbook.user.entity;

import com.clicknbook.artist.entity.Artist;
import com.clicknbook.customer.entity.Customer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@NamedQueries({
        @NamedQuery(name = UserSetting.FIND_BY_USER_USERNAME, query = "SELECT ua FROM UserSetting ua WHERE ua.username = :username")
})
@Entity
@Table(name = "user_setting")
@NoArgsConstructor
@Setter
@Getter
public class UserSetting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    @ManyToOne
    @JoinColumn(name = "default_artist_id")
    private Artist defaultArtist;
    @ManyToOne
    @JoinColumn(name = "default_customer_id")
    private Customer defaultCustomer;
    @Column
    private String username;

    public static final String FIND_BY_USER_USERNAME = "UserSetting.FindByUsername";
}
