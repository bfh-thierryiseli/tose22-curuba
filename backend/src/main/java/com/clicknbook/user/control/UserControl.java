package com.clicknbook.user.control;

import com.clicknbook.artist.entity.Artist;
import com.clicknbook.customer.entity.Customer;
import com.clicknbook.user.entity.UserSetting;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Optional;

@ApplicationScoped
@Transactional
public class UserControl {
    @Inject
    EntityManager em;

    public UserSetting updateSetting(UserSetting data, String username) {
        Optional<UserSetting> userSettingOptional = em
                .createNamedQuery(UserSetting.FIND_BY_USER_USERNAME, UserSetting.class)
                .setParameter("username", username)
                .getResultList()
                .stream()
                .findFirst();
        UserSetting userSetting = userSettingOptional.orElseGet(UserSetting::new);
        userSetting.setUsername(username);
        if (data.getDefaultCustomer() != null) {
            userSetting.setDefaultCustomer(em.find(Customer.class, data.getDefaultCustomer().getId()));
        }
        if (data.getDefaultArtist() != null) {
            userSetting.setDefaultArtist(em.find(Artist.class, data.getDefaultArtist().getId()));
        }
        return em.merge(userSetting);
    }
}
