package com.clicknbook.user.boundary;

import com.clicknbook.user.control.UserControl;
import com.clicknbook.user.entity.UserArtist;
import com.clicknbook.user.entity.UserCustomer;
import com.clicknbook.user.entity.UserSetting;
import io.quarkus.security.identity.SecurityIdentity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    @Inject
    EntityManager em;
    @Inject
    UserControl userControl;
    @Inject
    SecurityIdentity securityIdentity;

    @GET
    @Path("{username}/customers")
    @Transactional
    public List<UserCustomer> getCustomers(@PathParam("username") String username) {
        return em
                .createNamedQuery(UserCustomer.FIND_BY_USER_USERNAME, UserCustomer.class)
                .setParameter("username", username)
                .getResultList();
    }

    @GET
    @Path("{username}/settings")
    @Transactional
    public List<UserSetting> getSettings(@PathParam("username") String username) {
        return em
                .createNamedQuery(UserSetting.FIND_BY_USER_USERNAME, UserSetting.class)
                .setParameter("username", username)
                .getResultList();
    }

    @PUT
    @Path("{username}/settings")
    @Transactional
    public Response putSettings(@PathParam("username") String username, UserSetting data) {
        String loggedUsername = securityIdentity.getPrincipal().getName();
        if (!username.equals(loggedUsername)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.ok().entity(userControl.updateSetting(data, username)).build();
    }

    @GET
    @Path("{username}/artists")
    @Transactional
    public List<UserArtist> getArtists(@PathParam("username") String username) {
        return em
                .createNamedQuery(UserArtist.FIND_BY_USER_USERNAME, UserArtist.class)
                .setParameter("username", username)
                .getResultList();
    }
}
