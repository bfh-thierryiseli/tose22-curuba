package com.clicknbook.dataservice.insert.boundary;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.Set;
import java.util.stream.Collectors;

@Path("inserts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InsertResource {
    @Inject
    EntityManager entityManager;

    @POST
    @Path("master-data")
    @Transactional
    public Response createMasterData() {
        create(buildSectionJson("Musik"));
        create(buildSectionJson("Tanz"));
        create(buildSectionJson("Comedy"));
        create(buildSectionJson("Magie"));
        return Response
                .created(null)
                .build();
    }

    @POST
    @Transactional
    public Response createOne(JsonObject jsonObject) {
        create(jsonObject);
        return Response
                .created(null)
                .build();
    }

    private JsonObject buildSectionJson(String name) {
        return Json.createObjectBuilder()
                .add("table", "section")
                .add("data",
                        Json.createObjectBuilder()
                                .add("name", name))
                .build();
    }

    private void create(JsonObject jsonObject) {
        Set<String> keys = jsonObject
                .getJsonObject("data")
                .keySet();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO ");
        stringBuilder.append(jsonObject.getString("table"));
        stringBuilder.append("(");
        stringBuilder.append(String.join(",", keys));
        stringBuilder.append(")");
        stringBuilder.append(" VALUES (");
        stringBuilder.append(keys
                .stream()
                .map(this::getKeyAsParameter)
                .collect(Collectors.joining(",")));
        stringBuilder.append(")");
        Query nativeQuery = entityManager.createNativeQuery(stringBuilder.toString());
        for (String key : keys) {
            JsonValue.ValueType valueType = jsonObject.getJsonObject("data").get(key).getValueType();
            if (valueType.equals(JsonValue.ValueType.STRING)) {
                if (isValidOffsetDateTime(jsonObject.getJsonObject("data").getString(key))) {
                    nativeQuery.setParameter(key, Timestamp.valueOf(OffsetDateTime.parse(jsonObject.getJsonObject("data").getString(key)).toLocalDateTime()), TemporalType.TIMESTAMP);
                    continue;
                }
            }
            nativeQuery.setParameter(key, getValue(jsonObject.getJsonObject("data"), key, valueType));
        }
        nativeQuery.executeUpdate();
    }

    private Object getValue(JsonObject jsonObject, String key, JsonValue.ValueType valueType) {
        if (valueType.equals(JsonValue.ValueType.STRING)) {
            return jsonObject.getString(key);
        } else if (valueType.equals(JsonValue.ValueType.NUMBER)) {
            return jsonObject.getJsonNumber(key).bigIntegerValue();
        } else if (valueType.equals(JsonValue.ValueType.NULL)) {
            return null;
        }
        return jsonObject.get(key).toString();
    }

    public boolean isValidOffsetDateTime(String date) {
        try {
            OffsetDateTime.parse(date);
        } catch (DateTimeParseException pe) {
            return false;
        }
        return true;
    }

    private String getKeyAsParameter(String key) {
        return ":" + key;
    }
}