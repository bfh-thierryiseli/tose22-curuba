package com.clicknbook.dataservice.delete.boundary;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("deletes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DeleteResource {
    @Inject
    EntityManager entityManager;

    @DELETE
    @Transactional
    public Response delete() {
        List<String> tablesToDelete = List.of(
                "Tag_Artist",
                "User_Artist",
                "User_Customer",
                "User_Setting",
                "Rating",
                "Reservation",
                "Artist",
                "Customer",
                "Address",
                "Section",
                "Tag");
        for (String table : tablesToDelete) {
            entityManager.createNativeQuery("DELETE FROM " + table).executeUpdate();
        }
        return Response.ok().build();
    }
}
