package com.clicknbook.dataservice.get.boundary;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("lists")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ListResource {
    @Inject
    EntityManager entityManager;

    @GET
    @Transactional
    public Response getReservations(@QueryParam("table") String table) {
        List list = entityManager.createNativeQuery("SELECT * FROM " + table).getResultList();
        return Response
                .ok()
                .entity(list)
                .build();
    }
}