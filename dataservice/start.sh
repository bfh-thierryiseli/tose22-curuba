#!/usr/bin/env bash

scriptdir="$(dirname "$0")"
cd "$scriptdir"

chmod +x ./mvnw
./mvnw clean
./mvnw compile quarkus:dev -Ddebug=5006 -Dquarkus.http.port=8081