# Setup

## Host or VM

Install Ubuntu Desktop 20.04 or higher in a host or on a VM with minimal 2 cpu-kernels and 4096 MB RAM.

## Ansible way

Ensure latest sources:

```bash
sudo apt-get update
```

Ansible needs to be installed:

```bash
sudo apt-get install ansible
```

Also git is needed:

```bash
sudo apt-get install git
```

For the next step you can checkout/clone this gitlab project to your desire location on your host or vm.

```bash
git clone https://gitlab.com/bfh-thierryiseli/tose22-curuba.git
```

Switch to `setup` folder and run following commands, ensure that your current user has sudo rights (ubuntu is in that case the username):

```bash
sudo usermod -a -G sudo "ubuntu"
cd tose22-curuba/setup
sudo ansible-galaxy install -r requirements.yml
sudo ansible-playbook playbook.yml
```

## Manual way

### IntelliJ

Install it with the toolbox [Link to IntelliJ Installation Guide](https://www.jetbrains.com/help/idea/installation-guide.html#2fa94614):

### Visual Studio Code

Easiest way is to install it with trusted source: 

```bash
sudo apt-get install wget gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
```

And then install necessary steps:

```bash
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install code
```

### OpenJDK 17

Install jdk also with apt-get:

```bash
sudo apt-get install openjdk-17-jdk
```

### Docker and Docker-Compose

Setup repository for docker:

```bash
sudo apt-get install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

The install it with apt-get:

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

### Maven

Maven can be also installed with apt-get:

```bash
sudo apt-get update
sudo apt-get install maven
```

### NodeJS

NodeJS needs to commands:

```bash
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install nodejs
```

### Chromium

Install chromium for testing and usage:

```bash
sudo apt-get install chromium-browser
```
