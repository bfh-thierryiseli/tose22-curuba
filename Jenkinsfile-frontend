def gitversion = []
def mainProject = "clickNbook"
def namespace = mainProject.toLowerCase()
def appName = "${namespace}-frontend"
def env = "dev"
def envUpperCase = env.toUpperCase()
def helmChartUrl = "https://github.com/98dots/helm-chart-javascript-frontend"
def devContainerRegistryHost = "registry.98dots.com"
def devContainerRegistry = "registry.98dots.com/images-dev"
def gitUrl = "https://gitlab.com/bfh-thierryiseli/tose22-curuba"
def dateFormat = new java.text.SimpleDateFormat("yyyyMMddHHmmss")
def date = new Date()
def timestamp = dateFormat.format(date)
def backendUrl = "https://dev.clicknbook.ch/clicknbook-backend"
def frontendUrl = "https://dev.clicknbook.ch"
def dataserviceUrl = "https://dev.clicknbook.ch/clicknbook-dataservice"

podTemplate(containers: [
        containerTemplate(name: 'docker', image: 'docker', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'helm', image: 'lachlanevenson/k8s-helm:v3.4.2', command: 'cat', ttyEnabled: true),
        containerTemplate(name: 'gitversion', image: 'im5tu/netcore-gitversion:3-alpine', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'npm', image: 'node:18.2.0-bullseye', command: 'cat', ttyEnabled: true),
        containerTemplate(name: 'maven', image: 'maven:3.8.5-openjdk-17', command: 'cat', ttyEnabled: true),
        containerTemplate(name: 'selenium-chrome', image: 'selenium/standalone-chrome:99.0', ttyEnabled: true, command: '', ports: [portMapping(containerPort: 4444)])],
        volumes: [
                hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
        ]) {
    node(POD_LABEL) {
        stage('Checkout') {
            checkout scm
        }

        container('gitversion') {
            stage('Set version') {
                def output = sh(returnStdout: true, script: "dotnet-gitversion /output json")
                gitversion = readJSON text: output
                currentBuild.displayName = "${gitversion.MajorMinorPatch} (${currentBuild.displayName})"
                echo output
            }
        }

        container('npm') {
            dir("frontend") { 
                stage('Build frontend') {
                    sh "./build.sh"
                }
            }
        }

        container('docker') {
            dir("frontend") { 
                stage('Build image') {
                    dir('build-docker') {
                        sh 'cp ../Dockerfile .'
                        sh 'cp -r ../build .'
                        sh "docker build -t ${appName}:v${gitversion.MajorMinorPatch} ."
                    }
                } 

                stage('Push to dev registry') {
                    withCredentials([usernamePassword(credentialsId: 'registry-98dots', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${devContainerRegistryHost}"
                    }
                    sh "docker image tag ${appName}:v${gitversion.MajorMinorPatch} ${devContainerRegistry}/${appName}:v${gitversion.MajorMinorPatch}"
                    sh "docker push ${devContainerRegistry}/${appName}:v${gitversion.MajorMinorPatch}"
                }
            } 
        }

        stage('Get config') {
            dir("frontend/config") {
                def config = readYaml file: "${envUpperCase}.yml"
                config.image.tag = "v${gitversion.MajorMinorPatch}"
                writeYaml file: "${envUpperCase}.yml", data: config, overwrite: true
            }
        }

        container('helm') {
            dir("frontend") { 
                stage('Deploy to dev') {
                    dir("helm-deploy") {
                        git url: "${helmChartUrl}"
                        sh "cp ../config/${envUpperCase}.yml ${envUpperCase}.yml"
                        sh "cp ../config/${envUpperCase}-config.json ./config.json"
                        sh "cp ../config/${envUpperCase}-nginx.conf ./nginx.conf"
                        sh "cp ../build/index.html ./index.html"
                        sh "helm upgrade -i ${appName} . -f ${envUpperCase}.yml -n ${namespace}-${env} --create-namespace --wait"
                    }
                }
            } 
        }

        container('maven') {
            stage('BDD-Tests') {
                dir('bdd') {
                    withCredentials([
                        usernamePassword(credentialsId: 'clicknbook-dev-user-test', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {   
                        withEnv([
                            "BROWSER=RemoteChrome",
                            "SCREENSHOTS=true",
                            "REMOTE_DRIVER_URL=http://localhost:4444/wd/hub",
                            "FRONTEND_URL=${frontendUrl}",
                            "BACKEND_URL=${backendUrl}",
                            "DATASERVICE_URL=${dataserviceUrl}",
                            "MAIL_HOG_URL=https://dev.clicknbook.ch/mailhog",
                            "SSO_TEST_USER=${USERNAME}",
                            "SSO_TEST_PASSWORD=${PASSWORD}"]) {
                            try {
                                sh "mvn clean package"
                            } catch(error) {
                                throw error
                            } finally {
                                junit allowEmptyResults: true, testResults: "**/target/surefire-reports/TEST-*.xml"
                                archiveArtifacts artifacts: 'target/cucumber-html-reports/**/*.*', fingerprint: true
                            }
                        }
                    }              
                }
            } 
        }
    }
}

