#!/usr/bin/env bash

scriptdir="$(dirname "$0")"
cd "$scriptdir"

BACKEND_URL=http://localhost:8080/clicknbook-backend \
DATASERVICE_URL=http://localhost:8081/clicknbook-dataservice \
FRONTEND_URL=http://localhost:3000 \
MAIL_HOG_URL=http://localhost:8025 \
BROWSER=LocalChrome \
SCREENSHOTS="true" \
SSO_URL=http://localhost:8181/auth \
SSO_REALM=clicknbook \
SSO_CLIENT=clicknbook-test \
SSO_SECRET=ae1fd6c6-5a91-4fd9-b040-2cd556914d1e \
SSO_TEST_USER=keycloak \
SSO_TEST_PASSWORD=test \
mvn clean package