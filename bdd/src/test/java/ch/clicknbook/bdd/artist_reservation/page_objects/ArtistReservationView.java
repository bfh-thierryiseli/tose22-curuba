package ch.clicknbook.bdd.artist_reservation.page_objects;

import ch.clicknbook.bdd.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.stream.Collectors;

public class ArtistReservationView extends BasePageObject {
    private final String urlBase;

    public ArtistReservationView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public ArtistReservationView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public List<String> getDetailsTextes() {
        return findElements(".reservation-detail")
                .stream()
                .map(x -> x.getShadowRoot().findElement(By.cssSelector("slot[name='summary']")).getText())
                .collect(Collectors.toList());
    }
}

