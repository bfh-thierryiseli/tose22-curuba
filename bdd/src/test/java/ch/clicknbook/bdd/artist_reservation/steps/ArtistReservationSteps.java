package ch.clicknbook.bdd.artist_reservation.steps;

import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.artist_reservation.page_objects.ArtistReservationView;
import ch.clicknbook.bdd.reservation_form.dtos.Reservation;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ArtistReservationSteps {
    private final ArtistReservationView artistReservationView;

    public ArtistReservationSteps(StepHooks stepHooks) {
        artistReservationView = new ArtistReservationView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Wenn("ich die Reservationen als Künstler einsehe")
    public void ichDieReservationenAlsKünstlerEinsehe() {
        artistReservationView.navigate("artist-reservation");
    }

    @Dann("sehe ich die vergangene Reservation")
    public void seheIchDieVergangeneReservation(Reservation expectedReservation) {
        List<String> getDetailTextes = artistReservationView.getDetailsTextes();
        Boolean reservationExists = getDetailTextes
                .stream()
                .anyMatch(x -> x.startsWith(expectedReservation.getName()));
        assertThat(reservationExists, is(true));
    }
}
