package ch.clicknbook.bdd.customer_home.steps;

import ch.clicknbook.bdd.DateTimeHelper;
import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.artist.dtos.Artist;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.customer_home.page_objects.CustomerHomeView;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Und;
import io.cucumber.java.de.Wenn;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static ch.clicknbook.bdd.DateTimeHelper.DATE_FORMATTER;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CustomerHomeSteps {
    private final CustomerHomeView customerHomeView;

    public CustomerHomeSteps(StepHooks stepHooks) {
        customerHomeView = new CustomerHomeView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Angenommen("ich befinde mich auf der Künstlerübersicht")
    public void ichBefindeMichAufDerKünstlerübersicht() {
        customerHomeView.navigate("customer");
    }

    @Dann("sehe ich den Künstler")
    public void seheIchDenKünstler(Artist expectedArtist) {
        List<ReadableElement> cardTitles = customerHomeView.getCardTitles();
        Boolean artistExists = cardTitles
                .stream()
                .anyMatch(x -> x.getValue().startsWith(expectedArtist.getName()));
        assertThat(artistExists, is(true));
    }

    @Wenn("ich nach {string} suche")
    public void ichNachSuche(String nameDescription) {
        customerHomeView.fillNameDescriptionFilter(nameDescription);
    }

    @Dann("sehe ich den Künstler nicht")
    public void seheIchDenKünstlerNicht(Artist expectedArtist) {
        List<ReadableElement> cardTitles = customerHomeView.getCardTitles();
        Boolean artistNotExists = cardTitles
                .stream()
                .noneMatch(x -> x.getValue().startsWith(expectedArtist.getName()));
        assertThat(artistNotExists, is(true));
    }

    @Wenn("ich nach dem Datum {string} filtere")
    public void ichNachDemDatumFiltere(String dateAsString) {
        OffsetDateTime date = DateTimeHelper.getDate(dateAsString);
        customerHomeView.fillReservationDateFilter(DATE_FORMATTER.format(date));
    }

    @Wenn("ich die Bewertungen von {string} betrachte")
    public void ichDieBewertungenVonBetrachte(String artistName) {
        customerHomeView.navigate("customer");
        customerHomeView.clickRatings(artistName);
    }

    @Wenn("die Bewertung {string} melde")
    public void dieBewertungMelde(String ratingText) {
        customerHomeView.clickReportRating(ratingText);
    }
}
