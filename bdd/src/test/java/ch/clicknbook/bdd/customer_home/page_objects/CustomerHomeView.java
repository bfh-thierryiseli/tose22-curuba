package ch.clicknbook.bdd.customer_home.page_objects;

import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CustomerHomeView extends BasePageObject {
    private final String urlBase;

    public CustomerHomeView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public CustomerHomeView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void clickRatings(String artistName) {
        ClickableElement buttonElement = findTypedElement("*[rating-artist-name='" + artistName.toLowerCase() + "']");
        buttonElement.click();
    }

    public List<ReadableElement> getCardTitles() {
        return findTypedElements(".list-item > h3");
    }

    public void fillNameDescriptionFilter(String nameDescription) {
        WriteableElement nameDescriptionElement = findTypedElement("*[id='filter-name-description']");
        nameDescriptionElement.setValue(nameDescription);
    }

    public void fillReservationDateFilter(String date) {
        WriteableElement dateElement = findTypedElement("*[id='filter-reservation-date']");
        dateElement.setValue(date);
    }

    public void clickReportRating(String ratingText) {
        List<WebElement> buttonElements = findElements("*[id='report-rating']");
        for (WebElement buttonElement : buttonElements) {
            WebElement textElement = (WebElement) ((JavascriptExecutor)getWebDriver()).executeScript("return arguments[0].nextElementSibling", buttonElement);
            if (textElement.getText().contains(ratingText)) {
                buttonElement.click();
            }
        }
    }
}
