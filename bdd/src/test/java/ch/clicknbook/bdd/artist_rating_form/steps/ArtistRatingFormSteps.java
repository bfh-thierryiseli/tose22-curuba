package ch.clicknbook.bdd.artist_rating_form.steps;

import ch.clicknbook.bdd.DataserviceService;
import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.artist_rating_form.dtos.Rating;
import ch.clicknbook.bdd.artist_rating_form.page_objects.ArtistRatingFormView;
import ch.clicknbook.bdd.base.ApiService;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Wenn;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.util.HashMap;

public class ArtistRatingFormSteps {
    public static final String ARTISTS_PUBLIC = "artists/public";
    private final ApiService artistPublicService;
    private final ApiService dataserviceListService;
    private final ApiService dataserviceInsertService;
    private final ArtistRatingFormView artistRatingFormView;

    public ArtistRatingFormSteps(StepHooks stepHooks) {
        artistRatingFormView = new ArtistRatingFormView(stepHooks.browserDriver, stepHooks.baseUrl);
        WebTarget backend = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("BACKEND_URL"));
        WebTarget dataservice = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("DATASERVICE_URL"));
        this.artistPublicService = new ApiService(
                backend,
                ARTISTS_PUBLIC);
        this.dataserviceListService = new ApiService(
                dataservice,
                DataserviceService.LISTS);
        this.dataserviceInsertService = new ApiService(
                dataservice,
                DataserviceService.INSERTS
        );
    }

    @Angenommen("ich befinde mich auf der Maske für die Bewertung von {string}")
    public void ichBefindeMichAufDerMaskeFürDieBewertungVon(String artistName) {
        HashMap<String, String> queryParams = new HashMap();
        queryParams.put("nameDescription", artistName);
        JsonObject artist = artistPublicService
                .getWithQueryParams(queryParams)
                .readEntity(JsonArray.class)
                .get(0)
                .asJsonObject();
        HashMap<String, String> listQueryParams = new HashMap();
        listQueryParams.put("table", "reservation");
        String reservationId = dataserviceListService
                .getWithQueryParams(listQueryParams)
                .readEntity(JsonArray.class)
                .get(0)
                .asJsonArray()
                .get(0)
                .toString();
        artistRatingFormView.navigate("artist-rating-form?artist-id=" + artist.getJsonNumber("id").bigIntegerValue() + "&reservation-id=" + reservationId);
    }

    @Wenn("ich die Bewertung mit folgenden Informationen ausfülle")
    public void ichDieBewertungMitFolgendenInformationenAusfülle(Rating rating) {
        artistRatingFormView.fillForm(rating);
        artistRatingFormView.submitForm();
    }

    @Angenommen("existiert folgende Bewertung")
    public void existiertFolgendeBewertung(Rating rating) {
        HashMap<String, String> queryParams = new HashMap();
        queryParams.put("nameDescription", rating.getArtistName());
        JsonObject artist = artistPublicService
                .getWithQueryParams(queryParams)
                .readEntity(JsonArray.class)
                .get(0)
                .asJsonObject();
        dataserviceInsertService.post(
                Json.createObjectBuilder()
                        .add("table", "rating")
                        .add("data", Json.createObjectBuilder()
                                .add("name", rating.getName())
                                .add("text", rating.getText())
                                .add("rating", Integer.parseInt(rating.getRating()))
                                .add("artist_id", artist.getJsonNumber("id").bigIntegerValue())
                                .build())
                        .build());
    }
}
