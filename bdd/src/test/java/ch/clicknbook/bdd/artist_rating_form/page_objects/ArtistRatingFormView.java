package ch.clicknbook.bdd.artist_rating_form.page_objects;

import ch.clicknbook.bdd.artist_rating_form.dtos.Rating;
import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class ArtistRatingFormView extends BasePageObject {
    private final String urlBase;

    public ArtistRatingFormView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public ArtistRatingFormView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void fillForm(Rating rating) {
        WriteableElement textField = findTypedElement("*[id='rating-text']");
        WriteableElement ratingField = findTypedElement("*[id='rating-rating']");

        textField.setValue(rating.getText());
        ratingField.setValue(rating.getRating());
    }

    public void submitForm() {
        ClickableElement submitElement = findTypedElement("*[id='submit-rating']");
        submitElement.click();
    }
}
