package ch.clicknbook.bdd.artist_rating_form.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Rating {
    private String text;
    @JsonAlias({"bewertung"})
    private String rating;
    private String name;
    @JsonAlias({"künstlername"})
    private String artistName;
}
