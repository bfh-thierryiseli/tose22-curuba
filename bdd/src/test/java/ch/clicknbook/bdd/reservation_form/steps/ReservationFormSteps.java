package ch.clicknbook.bdd.reservation_form.steps;

import ch.clicknbook.bdd.DataserviceService;
import ch.clicknbook.bdd.DateTimeHelper;
import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.base.ApiService;
import ch.clicknbook.bdd.reservation_form.dtos.Reservation;
import ch.clicknbook.bdd.reservation_form.page_objects.ReservationFormView;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.time.OffsetDateTime;
import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ReservationFormSteps {
    public static final String ARTISTS_PUBLIC = "artists/public";

    private final ReservationFormView reservationFormView;
    private final ApiService artistPublicService;
    private final ApiService dataserviceSection;


    public ReservationFormSteps(StepHooks stepHooks) {
        this.reservationFormView = new ReservationFormView(stepHooks.browserDriver, stepHooks.baseUrl);
        WebTarget backend = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("BACKEND_URL"));
        WebTarget dataservice = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("DATASERVICE_URL"));
        this.artistPublicService = new ApiService(
                backend,
                ARTISTS_PUBLIC);
        this.dataserviceSection = new ApiService(
                dataservice,
                DataserviceService.INSERTS);
    }

    @Angenommen("ich befinde mich auf der Buchungsseite für {string}")
    public void ichBefindeMichAufDerBuchungsseiteFür(String artistName) {
        HashMap<String, String> queryParams = new HashMap();
        queryParams.put("nameDescription", artistName);
        JsonObject artist = artistPublicService
                .getWithQueryParams(queryParams)
                .readEntity(JsonArray.class)
                .get(0)
                .asJsonObject();
        this.reservationFormView.navigate(
                "reservation-form?artist-id=" + artist.getJsonNumber("id").bigIntegerValue() + "&start-date=&end-date=");
    }

    @Wenn("ich folgende Reservation vornehme:")
    public void ichFolgendeReservationVornehme(Reservation reservation) {
        reservationFormView.fillForm(reservation);
        reservationFormView.submitForm();
    }

    @Dann("sehe ich die Reservetionsdetails für {string}")
    public void seheIchDieReservetionsdetailsFür(String expectedArtistName) {
        assertThat(reservationFormView.getArtistNameOfReservation(), is(expectedArtistName));
    }

    @Angenommen("es existieren folgende Reservationen")
    public void esExistierenFolgendeReservationen(Reservation reservation) {
        HashMap<String, String> queryParams = new HashMap();
        queryParams.put("nameDescription", reservation.getArtistName());
        JsonObject artist = artistPublicService
                .getWithQueryParams(queryParams)
                .readEntity(JsonArray.class)
                .get(0)
                .asJsonObject();

        OffsetDateTime start = DateTimeHelper.getDate(reservation.getReservationDate());
        start = start.withHour(Integer.parseInt(reservation.getFromTime().split(":")[0]));
        start = start.withMinute(Integer.parseInt(reservation.getFromTime().split(":")[1]));

        OffsetDateTime end = DateTimeHelper.getDate(reservation.getReservationDate());
        end = end.withHour(Integer.parseInt(reservation.getToTime().split(":")[0]));
        end = end.withMinute(Integer.parseInt(reservation.getToTime().split(":")[1]));
        dataserviceSection.post(
                Json.createObjectBuilder()
                        .add("table", "reservation")
                        .add("data", Json.createObjectBuilder()
                                .add("name", reservation.getName())
                                .add("email", reservation.getEmail())
                                .add("street", reservation.getStreet())
                                .add("status", Integer.parseInt(reservation.getStatus()))
                                .add("postalCode", reservation.getPostalCode())
                                .add("town", reservation.getTown())
                                .add("startAt", start.toString())
                                .add("endAt", end.toString())
                                .add("artist_id", artist.getJsonNumber("id").bigIntegerValue())
                                .build())
                        .build());
    }
}
