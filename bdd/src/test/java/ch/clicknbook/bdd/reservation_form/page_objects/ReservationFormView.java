package ch.clicknbook.bdd.reservation_form.page_objects;

import ch.clicknbook.bdd.DateTimeHelper;
import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import ch.clicknbook.bdd.reservation_form.dtos.Reservation;
import org.openqa.selenium.WebDriver;

import static ch.clicknbook.bdd.DateTimeHelper.DATE_FORMATTER;

public class ReservationFormView extends BasePageObject {
    private final String urlBase;

    public ReservationFormView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public ReservationFormView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void fillForm(Reservation reservation) {
        WriteableElement nameField = findTypedElement("*[id='reservation-name']");
        WriteableElement emailField = findTypedElement("*[id='reservation-email']");
        WriteableElement streetField = findTypedElement("*[id='reservation-street']");
        WriteableElement postalCodeField = findTypedElement("*[id='reservation-postalcode']");
        WriteableElement townField = findTypedElement("*[id='reservation-town']");
        WriteableElement dateField = findTypedElement("*[id='reservation-reservationdate']");
        WriteableElement fromTimeHour = findTypedElement("*[id='reservation-fromtimehour']");
        WriteableElement fromTimeMinute = findTypedElement("*[id='reservation-fromtimeminute']");
        WriteableElement toTimeHour = findTypedElement("*[id='reservation-totimehour']");
        WriteableElement toTimeMinute = findTypedElement("*[id='reservation-totimeminute']");

        nameField.setValue(reservation.getName());
        emailField.setValue(reservation.getEmail());
        streetField.setValue(reservation.getStreet());
        postalCodeField.setValue(reservation.getPostalCode());
        townField.setValue(reservation.getTown());
        dateField.setValue(DATE_FORMATTER.format(DateTimeHelper.getDate(reservation.getReservationDate())));
        fromTimeHour.setValue(reservation.getFromTime().split(":")[0]);
        fromTimeMinute.setValue(reservation.getFromTime().split(":")[1]);
        toTimeHour.setValue(reservation.getToTime().split(":")[0]);
        toTimeMinute.setValue(reservation.getToTime().split(":")[1]);
    }

    public void submitForm() {
        ClickableElement submitElement = findTypedElement("*[id='submit-reservation']");
        submitElement.click();
    }

    public String getArtistNameOfReservation() {
        ReadableElement artistNameElement = findTypedElement("*[id='reservation-artist-name']");
        return artistNameElement.getValue();
    }
}

