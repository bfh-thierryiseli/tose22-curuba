package ch.clicknbook.bdd.reservation_form.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Reservation {
    private String name;
    private String email;
    @JsonAlias({"strasseUndNummer(Veranstaltungsort)"})
    private String street;
    @JsonAlias({"postleitzahl(Veranstaltungsort)"})
    private String postalCode;
    @JsonAlias({"ort(Veranstaltungsort)"})
    private String town;
    @JsonAlias({"reservationdatum"})
    private String reservationDate;
    @JsonAlias({"von"})
    private String fromTime;
    @JsonAlias({"bis"})
    private String toTime;
    @JsonAlias({"status"})
    private String status;
    @JsonAlias({"künstler"})
    private String artistName;
    @JsonAlias({"bewertung"})
    private String rating;
}
