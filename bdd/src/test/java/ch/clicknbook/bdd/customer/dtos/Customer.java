package ch.clicknbook.bdd.customer.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Customer {
    @JsonAlias({"name"})
    private String name;
    @JsonAlias({"präferenz"})
    private String preference;
    @JsonAlias({"beschreibung"})
    private String description;
    @JsonAlias({"email"})
    private String email;
    @JsonAlias({"strasseUndNummer"})
    private String street;
    @JsonAlias({"postleitzahl"})
    private String postalCode;
    @JsonAlias({"ort"})
    private String town;
}
