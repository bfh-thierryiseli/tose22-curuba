package ch.clicknbook.bdd.availability.page_objects;

import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import org.openqa.selenium.WebDriver;

import java.time.OffsetDateTime;
import java.util.List;

import static ch.clicknbook.bdd.DateTimeHelper.DATE_FORMATTER;

public class AvailabilityView extends BasePageObject {
    private final String urlBase;

    public AvailabilityView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public AvailabilityView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void fillForm(OffsetDateTime from, OffsetDateTime to) {
        WriteableElement fromElement = findTypedElement("*[id='availability-from']");
        WriteableElement toElement = findTypedElement("*[id='availability-to']");
        fromElement.setValue(DATE_FORMATTER.format(from));
        toElement.setValue(DATE_FORMATTER.format(to));
    }

    public void submitCreateForm() {
        ClickableElement submitElement = findTypedElement("*[id='availability-create-submit']");
        submitElement.click();
    }

    public void submitDeleteForm() {
        ClickableElement submitElement = findTypedElement("*[id='availability-delete-submit']");
        submitElement.click();
    }
}
