package ch.clicknbook.bdd.availability.steps;

import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.availability.page_objects.AvailabilityView;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Und;
import io.cucumber.java.de.Wenn;

import java.time.LocalDate;
import java.time.OffsetDateTime;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AvailabilitySteps {
    private final AvailabilityView availabilityView;

    public AvailabilitySteps(StepHooks stepHooks) {
        availabilityView = new AvailabilityView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Und("ich befinde mich auf der Maske fürs Eintragen der Verfügbarkeiten")
    public void ichBefindeMichAufDerMaskeFürsEintragenDerVerfügbarkeiten() {
        availabilityView.navigate("artist-availability");
    }

    @Wenn("ich Verfügbarkeit für die nächsten zwei Wochen eintrage")
    public void ichVerfügbarkeitFürDieNächstenZweiWochenEintrage() {
        availabilityView.fillForm(OffsetDateTime.now().plusDays(1), OffsetDateTime.now().plusWeeks(2));
        availabilityView.submitCreateForm();
    }

    @Wenn("ich Verfügbarkeit für die nächsten zwei Wochen lösche")
    public void ichVerfügbarkeitFürDieNächstenZweiWochenLösche() {
        availabilityView.fillForm(OffsetDateTime.now().plusDays(1), OffsetDateTime.now().plusWeeks(2));
        availabilityView.submitDeleteForm();
    }
}
