package ch.clicknbook.bdd.artist_selection.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Tag {
    private String name;
}
