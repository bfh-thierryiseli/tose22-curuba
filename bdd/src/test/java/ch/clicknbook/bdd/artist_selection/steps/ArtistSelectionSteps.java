package ch.clicknbook.bdd.artist_selection.steps;

import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.artist_selection.dtos.Tag;
import ch.clicknbook.bdd.artist_selection.page_objects.ArtistSelectionView;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class ArtistSelectionSteps {
    private final ArtistSelectionView artistSelectionView;

    public ArtistSelectionSteps(StepHooks stepHooks) {
        artistSelectionView = new ArtistSelectionView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Angenommen("ich befinde mich auf den Künstlerprofilen")
    public void ichBefindeMichAufDenKünstlerprofilen() {
        artistSelectionView.navigate("artist-selection");
    }

    @Wenn("ich den Künstler {string} mit folgenden Stichwörter ergänze")
    public void ichDenKünstlerMitFolgendenStichwörterErgänze(String artistName, List<Tag> tags) {
        artistSelectionView.addTags(artistName, tags);
    }

    @Dann("sehe ich bei {string} die definierten Stichwörter")
    public void seheIchBeiDieDefiniertenStichwörter(String artistName, List<Tag> tags) {
        assertThat(artistSelectionView.getTags(artistName), containsInAnyOrder(tags.toArray()));
    }
}
