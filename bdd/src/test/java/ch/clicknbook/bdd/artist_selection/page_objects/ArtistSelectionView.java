package ch.clicknbook.bdd.artist_selection.page_objects;

import ch.clicknbook.bdd.artist_selection.dtos.Tag;
import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import ch.clicknbook.bdd.customer.dtos.Customer;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.stream.Collectors;

public class ArtistSelectionView extends BasePageObject {
    private final String urlBase;

    public ArtistSelectionView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public ArtistSelectionView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void fillForm(Customer customer) {
        WriteableElement nameField = findTypedElement("*[id='customer-name']");
        WriteableElement descriptionField = findTypedElement("*[id='customer-description']");
        WriteableElement sectionField = findTypedElement("*[id='customer-preference']");
        WriteableElement emailField = findTypedElement("*[id='customer-email']");
        WriteableElement streetField = findTypedElement("*[id='customer-street']");
        WriteableElement postalCodeField = findTypedElement("*[id='customer-postal-code']");
        WriteableElement townField = findTypedElement("*[id='customer-town']");

        nameField.setValue(customer.getName());
        descriptionField.setValue(customer.getDescription());
        sectionField.setValue(customer.getPreference());
        emailField.setValue(customer.getEmail());
        streetField.setValue(customer.getStreet());
        postalCodeField.setValue(customer.getPostalCode());
        townField.setValue(customer.getTown());
    }

    public void submitForm() {
        ClickableElement submitElement = findTypedElement("*[id='submit-customer']");
        submitElement.click();
    }

    public List<ReadableElement> getCardTitles() {
        return findTypedElements(".list-item > h3");
    }

    public void addTags(String artistName, List<Tag> tags) {
        WriteableElement tagElement = findTypedElement("*[artist-name='" + artistName.toLowerCase() + "'] .new-tag");
        for (Tag tag : tags) {
            tagElement.setValue(tag.getName() + Keys.chord(Keys.ENTER));
        }
    }

    public List<Tag> getTags(String artistName) {
        List<ReadableElement> tags = findTypedElements("*[artist-name='" + artistName.toLowerCase() + "'] .tag");
        return tags.stream().map(tag -> new Tag(tag.getValue())).collect(Collectors.toList());
    }
}
