package ch.clicknbook.bdd.artist_contact_form.steps;

import ch.clicknbook.bdd.DataserviceService;
import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.artist_contact_form.dtos.Contact;
import ch.clicknbook.bdd.artist_contact_form.page_objects.ContactFormView;
import ch.clicknbook.bdd.base.ApiService;
import ch.clicknbook.bdd.main.dtos.Mail;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.util.HashMap;

public class ContactFormSteps {
    public static final String ARTISTS_PUBLIC = "artists/public";

    private final ContactFormView contactFormView;
    private final ApiService artistPublicService;
    private final ApiService dataserviceSection;


    public ContactFormSteps(StepHooks stepHooks) {
        this.contactFormView = new ContactFormView(stepHooks.browserDriver, stepHooks.baseUrl);
        WebTarget backend = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("BACKEND_URL"));
        WebTarget dataservice = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("DATASERVICE_URL"));
        this.artistPublicService = new ApiService(
                backend,
                ARTISTS_PUBLIC);
        this.dataserviceSection = new ApiService(
                dataservice,
                DataserviceService.INSERTS);
    }

    @Angenommen("ich befinde mich auf der Kontaktseite für {string}")
    public void ichBefindeMichAufDerKontaktseiteFür(String artistName) {
        HashMap<String, String> queryParams = new HashMap();
        queryParams.put("nameDescription", artistName);
        JsonObject artist = artistPublicService
                .getWithQueryParams(queryParams)
                .readEntity(JsonArray.class)
                .get(0)
                .asJsonObject();
        this.contactFormView.navigate(
                "artist-contact-form?artist-id=" + artist.getJsonNumber("id").bigIntegerValue() + "&start-date=&end-date=");
    }

    @Wenn("ich folgende Kontaktanfrage sende:")
    public void ichFolgendeKontaktanfrageSende(Contact contact) {
        contactFormView.fillForm(contact);
        contactFormView.submitForm();
    }

    @Dann("erhält der Künstler {string} die folgende Email")
    public void erhältDerKünstlerDieFolgendeEmail(String artistName, Mail expectedMail) {
        // TODO: Mails prüfen über MailHog noch machen
    }
}
