package ch.clicknbook.bdd.artist_contact_form.page_objects;

import ch.clicknbook.bdd.artist_contact_form.dtos.Contact;
import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import org.openqa.selenium.WebDriver;

public class ContactFormView extends BasePageObject {
    private final String urlBase;

    public ContactFormView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public ContactFormView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void fillForm(Contact contact) {
        WriteableElement nameField = findTypedElement("*[id='contact-name']");
        WriteableElement emailField = findTypedElement("*[id='contact-email']");
        WriteableElement streetField = findTypedElement("*[id='contact-street']");
        WriteableElement postalCodeField = findTypedElement("*[id='contact-postal-code']");
        WriteableElement townField = findTypedElement("*[id='contact-town']");
        WriteableElement messageField = findTypedElement("*[id='contact-message']");

        nameField.setValue(contact.getName());
        emailField.setValue(contact.getEmail());
        streetField.setValue(contact.getStreet());
        postalCodeField.setValue(contact.getPostalCode());
        townField.setValue(contact.getTown());
        messageField.setValue(contact.getMessage());
    }

    public void submitForm() {
        ClickableElement submitElement = findTypedElement("*[id='submit-contact']");
        submitElement.click();
    }
}

