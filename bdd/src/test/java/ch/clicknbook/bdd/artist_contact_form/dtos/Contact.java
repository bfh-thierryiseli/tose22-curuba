package ch.clicknbook.bdd.artist_contact_form.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contact {
    private String name;
    private String email;
    @JsonAlias({"strasseUndNummer"})
    private String street;
    @JsonAlias({"postleitzahl"})
    private String postalCode;
    @JsonAlias({"ort"})
    private String town;
    @JsonAlias({"fragenOderAnliegen"})
    private String message;
}
