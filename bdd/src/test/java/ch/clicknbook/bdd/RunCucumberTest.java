package ch.clicknbook.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.sorting.SortingMethod;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/report/cucumber.json"},
        features = "src/test/resources")
public class RunCucumberTest {
    @AfterClass
    public static void AfterAllScenarios() {
        File reportOutputDirectory = new File("target");
        List<String> jsonFiles = new ArrayList<>();
        jsonFiles.add("target/report/cucumber.json");

        String projectName = "click & book";
        Configuration configuration = new Configuration(reportOutputDirectory, projectName);

        configuration.setSortingMethod(SortingMethod.NATURAL);

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
    }
}
