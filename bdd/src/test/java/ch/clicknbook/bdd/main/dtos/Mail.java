package ch.clicknbook.bdd.main.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mail {
    @JsonAlias({"betreff"})
    private String subject;
    @JsonAlias({"inhalt"})
    private String content;
}
