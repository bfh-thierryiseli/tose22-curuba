package ch.clicknbook.bdd.main.page_objects;

import ch.clicknbook.bdd.User;
import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class MainView extends BasePageObject {
    private final String urlBase;

    public MainView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public MainView navigate() {
        webDriver.navigate().to(urlBase);
        return this;
    }

    public ReadableElement getEntranceTitle() {
        return findTypedElement(".main-title");
    }

    public List<ClickableElement> getEntranceButtons() {
        return findTypedElements("sl-button");
    }

    public ReadableElement getTitle() {
        return findTypedElement("h1");
    }

    public void login(User user) {
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        wait.until(x -> {
            try {
                ((JavascriptExecutor) webDriver).executeScript("window.keycloak.login()");
            } catch (JavascriptException e) {
                return false;
            }
            return true;
        });
        WriteableElement usernameElement = findTypedElement("*[id='username']");
        WriteableElement passwordElement = findTypedElement("*[id='password']");
        ClickableElement submitElement = findTypedElement("*[id='kc-login']");
        usernameElement.setValue(user.getUsername());
        passwordElement.setValue(user.getPassword());
        submitElement.click();
    }

    public String getSuccessAlertMessage() {
        ReadableElement alertMessage = findTypedElement("sl-alert[variant='success']");
        return alertMessage.getValue();
    }
}
