package ch.clicknbook.bdd.main.steps;

import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.User;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.main.page_objects.MainView;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;

public class MainSteps {
    private final MainView mainView;

    public MainSteps(StepHooks stepHooks) {
        mainView = new MainView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Angenommen("ich befinde mich auf der Hauptseite")
    public void ichBefindeMichAufDerHauptseite() {
        mainView.navigate();
    }

    @Dann("sehe ich den Seiteneinstieg mit dem Titel {string}")
    public void seheIchDenSeiteneinstiegMitDemTitel(String expectedTitle) {
        assertThat(mainView.getEntranceTitle().getValue(), is(expectedTitle));
    }

    @Wenn("ich als {string} einsteige")
    public void ichAlsEinsteige(String einstiegsText) {
        Optional<ClickableElement> clickableElement = mainView
                .getEntranceButtons()
                .stream()
                .filter(x -> x.getValue().equals(einstiegsText))
                .findFirst();
        clickableElement.ifPresent(ClickableElement::click);
    }

    @Dann("sehe ich die Seite mit dem Titel {string}")
    public void seheIchDieSeiteMitDemTitel(String expectedTitle) {
        assertThat(mainView.getTitle().getValue(), is(expectedTitle));
    }

    @Angenommen("ich bin als {string} eingeloggt")
    public void ichBinAlsEingeloggt(String username) {
        mainView.navigate();
        mainView.login(User.valueOf(username));
    }

    @Dann("sehe ich die Erfolgsmeldung {string}")
    public void seheIchDieErfolgsmeldung(String expectedAlertMessage) {
        assertThat(mainView.getSuccessAlertMessage(), is(expectedAlertMessage));
    }

    @Dann("sehe ich die Erfolgsinfo {string}")
    public void seheIchDieErfolgsinfo(String expectedSuccessInfo) {
        assertThat(mainView.getSuccessAlertMessage(), matchesPattern(expectedSuccessInfo));
    }
}
