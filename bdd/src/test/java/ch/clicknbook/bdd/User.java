package ch.clicknbook.bdd;

import lombok.Getter;

@Getter
public enum User {
    Test(System.getenv("SSO_TEST_USER"),
            System.getenv("SSO_TEST_PASSWORD"));

    private final String username;
    private final String password;

    User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
