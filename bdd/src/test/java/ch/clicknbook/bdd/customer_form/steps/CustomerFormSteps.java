package ch.clicknbook.bdd.customer_form.steps;

import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.customer.dtos.Customer;
import ch.clicknbook.bdd.customer_form.page_objects.CustomerFormView;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CustomerFormSteps {
    private final CustomerFormView customerFormView;

    public CustomerFormSteps(StepHooks stepHooks) {
        customerFormView = new CustomerFormView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Angenommen("ich befinde mich auf der Maske für die Erstellung eines Kundenprofil")
    public void ichBefindeMichAufDerMaskeFürDieErstellungEinesKundenprofil() {
        customerFormView.navigate("customer-form");
    }

    @Wenn("ich ein Kundenprofil mit folgenden Informationen erstelle")
    public void ichEinKundenprofilMitFolgendenInformationenErstelle(Customer customer) {
        customerFormView.fillForm(customer);
        customerFormView.submitForm();
    }

    @Dann("sehe ich das Kundenprofil")
    public void seheIchDasKundenprofil(Customer expectedCustomer) {
        List<ReadableElement> cardTitles = customerFormView.getCardTitles();
        Boolean customerExists = cardTitles
                .stream()
                .anyMatch(x -> x.getValue().startsWith(expectedCustomer.getName()));
        assertThat(customerExists, is(true));
    }
}
