package ch.clicknbook.bdd;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeHelper {
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MMddyyyy");

    public static OffsetDateTime getDate(String text) {
        if (text.startsWith("heute")) {
            String suffix = text.replace("heute", "");
            String symbol = suffix.substring(0, 1);
            String what = suffix.replace(symbol, "");
            if (symbol.equals("+")) {
                if (what.contains("tag")) {
                    int countOfWhat = Integer.parseInt(what.replace("tag", ""));
                    return OffsetDateTime.now().plusDays(countOfWhat);
                }
            } else if (symbol.equals("-")) {
                if (what.contains("tag")) {
                    int countOfWhat = Integer.parseInt(what.replace("tag", ""));
                    return OffsetDateTime.now().minusDays(countOfWhat);
                }
            }
        }
        return OffsetDateTime.now();
    }
}
