package ch.clicknbook.bdd.artist_form.steps;

import ch.clicknbook.bdd.StepHooks;
import ch.clicknbook.bdd.artist.dtos.Artist;
import ch.clicknbook.bdd.artist_form.page_objects.ArtistFormView;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import io.cucumber.java.de.Angenommen;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Wenn;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ArtistFormSteps {
    private final ArtistFormView artistFormView;

    public ArtistFormSteps(StepHooks stepHooks) {
        artistFormView = new ArtistFormView(stepHooks.browserDriver, stepHooks.baseUrl);
    }

    @Angenommen("ich befinde mich auf der Maske für die Erstellung eines Künstlerprofils")
    public void ichBefindeMichAufDerMaskeFürDieErstellungEinesKünstlerprofils() {
        artistFormView.navigate("artist-form");
    }

    @Wenn("ich ein Künstlerprofil mit folgenden Informationen erstelle")
    public void ichEinKünstlerprofilMitFolgendenInformationenErstelle(Artist artist) {
        artistFormView.fillForm(artist);
        artistFormView.submitForm();
    }

    @Dann("sehe ich das Künstlerprofil")
    public void seheIchDasKünstlerprofil(Artist expectedArtist) {
        List<ReadableElement> cardTitles = artistFormView.getCardTitles();
        Boolean artistExists = cardTitles
                .stream()
                .anyMatch(x -> x.getValue().startsWith(expectedArtist.getName()));
        assertThat(artistExists, is(true));
    }
}
