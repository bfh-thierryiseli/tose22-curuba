package ch.clicknbook.bdd.artist_form.page_objects;

import ch.clicknbook.bdd.artist.dtos.Artist;
import ch.clicknbook.bdd.base.BasePageObject;
import ch.clicknbook.bdd.base.web_elements.ClickableElement;
import ch.clicknbook.bdd.base.web_elements.ReadableElement;
import ch.clicknbook.bdd.base.web_elements.WriteableElement;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class ArtistFormView extends BasePageObject {
    private final String urlBase;

    public ArtistFormView(WebDriver webDriver, String urlBase) {
        super(webDriver);
        this.urlBase = urlBase;
    }

    public ArtistFormView navigate(String path) {
        webDriver.navigate().to(urlBase + "/" + path);
        return this;
    }

    public void fillForm(Artist artist) {
        WriteableElement nameField = findTypedElement("*[id='artist-name']");
        WriteableElement descriptionField = findTypedElement("*[id='artist-description']");
        WriteableElement sectionField = findTypedElement("*[id='artist-section']");
        WriteableElement emailField = findTypedElement("*[id='artist-email']");
        WriteableElement streetField = findTypedElement("*[id='artist-street']");
        WriteableElement postalCodeField = findTypedElement("*[id='artist-postal-code']");
        WriteableElement townField = findTypedElement("*[id='artist-town']");

        nameField.setValue(artist.getName());
        descriptionField.setValue(artist.getDescription());
        sectionField.setValue(artist.getSection());
        emailField.setValue(artist.getEmail());
        streetField.setValue(artist.getStreet());
        postalCodeField.setValue(artist.getPostalCode());
        townField.setValue(artist.getTown());
    }

    public void submitForm() {
        ClickableElement submitElement = findTypedElement("*[id='submit-artist']");
        submitElement.click();
    }

    public List<ReadableElement> getCardTitles() {
        return findTypedElements(".list-item > h3");
    }
}
