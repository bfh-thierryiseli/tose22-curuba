package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HtmlTextarea extends ElementBase implements WriteableElement {
    protected final static Logger LOGGER = Logger.getLogger(HtmlTextarea.class.getName());

    @Override
    public String getValue() {
        return getValue(getWebElement());
    }

    @Override
    public void setValue(String value) {
        setValue(value, 0);
    }

    private void setValue(String value, int simulateDelayInMillis) {
        WebElement element = getWebElement();
        element.clear();
        if (simulateDelayInMillis > 0) {
            for (int i = 0; i < value.length(); i++) {
                element.sendKeys(value.substring(i, i + 1));
                try {
                    TimeUnit.MILLISECONDS.sleep(simulateDelayInMillis);
                } catch (InterruptedException interruptedException) {
                    LOGGER.log(Level.WARNING, "Waiting failed!", interruptedException);
                    Thread.currentThread().interrupt();
                }
            }
        } else {
            element.sendKeys(value);
        }
    }

    private String getValue(WebElement element) {
        return element.getAttribute("value");
    }
}
