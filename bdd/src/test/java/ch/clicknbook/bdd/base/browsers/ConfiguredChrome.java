package ch.clicknbook.bdd.base.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConfiguredChrome {
    public static WebDriver getRemoteChrome() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            try {
                return new RemoteWebDriver(new URL(System.getenv("REMOTE_DRIVER_URL")), getChromeOptions());
            } catch (WebDriverException | MalformedURLException e) {
                System.out.println(String.format("Error connecting to %s: %s. Retrying", System.getenv("REMOTE_DRIVER_URL"), e));
                Thread.sleep(1000);
            }
        }
        throw new
                RuntimeException("Connection of remote driver failed!");
    }
    public static WebDriver getChrome() {
        System.setProperty("SNAP_PATH", "/snap/bin/chromium.chromedriver");
        return new ChromeDriver(
                (new ChromeDriverService.Builder() {
                    @Override
                    protected File findDefaultExecutable() {
                        if (new File(System.getProperty("SNAP_PATH")).exists()) {
                            return new File(System.getProperty("SNAP_PATH")) {
                                @Override
                                public String getCanonicalPath() {
                                    return this.getAbsolutePath();
                                }
                            };
                        } else {
                            return super.findDefaultExecutable();
                        }
                    }
                }).build(), getChromeOptions());
    }

    public static WebDriver getChromeHeadless() {
        ChromeOptions headlessChromeOptions = getChromeOptions();
        headlessChromeOptions.addArguments("headless");
        return new ChromeDriver(headlessChromeOptions);
    }

    private static ChromeOptions getChromeOptions() {
        Map<String, Object> prefs = new LinkedHashMap<>();
        prefs.put("disable-popup-blocking", true);

        ChromeOptions options = new ChromeOptions();
        options.addArguments(CapabilityType.ACCEPT_INSECURE_CERTS, "true");
        options.addArguments("--no-sandbox");
        options.addArguments("--remote-debugging-port=9999");
        options.addArguments("whitelist-ip $*");
        options.addArguments("--ignore-gpu-blocklist");
        options.addArguments("--ignore-ssl-errors=yes");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-features=VizDisplayCompositor");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--window-size=1920,1080");
        options.setExperimentalOption("prefs", prefs);

        return options;
    }
}
