package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.JavascriptExecutor;

public class SlRating extends ElementBase implements WriteableElement {
    @Override
    public String getValue() {
        return getWebElement().getAttribute("value");
    }

    @Override
    public void setValue(String value) {
        ((JavascriptExecutor) getBasePageObject().getWebDriver())
                .executeScript("arguments[0].value = arguments[1]", getWebElement(), value);
    }
}
