package ch.clicknbook.bdd.base.web_elements;

public interface TypedWebElement {
    boolean isDisabled();
}
