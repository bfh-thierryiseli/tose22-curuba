package ch.clicknbook.bdd.base.web_elements;

public interface WriteableElement extends ReadableElement {
    void setValue(String value);
}
