package ch.clicknbook.bdd.base.web_elements;

public class HtmlInput extends ElementBase implements WriteableElement {
    @Override
    public String getValue() {
        return getWebElement().getAttribute("value");
    }

    @Override
    public void setValue(String value) {
        getWebElement().clear();
        getWebElement().sendKeys(value);
    }
}
