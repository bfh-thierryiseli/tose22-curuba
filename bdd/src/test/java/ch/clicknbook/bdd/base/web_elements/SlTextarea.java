package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SlTextarea extends ElementBase implements WriteableElement {
    @Override
    public String getValue() {
        return getWebElement().getAttribute("value");
    }

    @Override
    public void setValue(String value) {
        WebElement textarea = getWebElement().getShadowRoot().findElement(By.cssSelector("textarea"));
        textarea.clear();
        textarea.sendKeys(value);
    }
}
