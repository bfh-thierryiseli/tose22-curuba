package ch.clicknbook.bdd.base.web_elements;


import ch.clicknbook.bdd.base.BasePageObject;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebElement;

public class ElementBase {
    @Getter
    @Setter
    private WebElement webElement;
    @Getter
    @Setter
    private BasePageObject basePageObject;

    public ElementBase() {
    }

    public boolean isDisabled() {
        return !getWebElement().isEnabled();
    }
}
