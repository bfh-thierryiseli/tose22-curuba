package ch.clicknbook.bdd.base.web_elements;

public class HtmlLink extends ElementBase implements ReadableElement, ClickableElement {

    @Override
    public void click() {
        getWebElement().click();
    }

    @Override
    public String getValue() {
        return getWebElement().getText();
    }

    public String getHref() {
        return getWebElement().getAttribute("href");
    }
}
