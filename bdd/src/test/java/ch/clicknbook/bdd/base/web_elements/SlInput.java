package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class SlInput extends ElementBase implements WriteableElement {
    @Override
    public String getValue() {
        return getWebElement().getAttribute("value");
    }

    @Override
    public void setValue(String value) {
        WebElement input = getWebElement().getShadowRoot().findElement(By.cssSelector("input"));
        input.clear();
        input.sendKeys(value);
        ((JavascriptExecutor) getBasePageObject().getWebDriver())
                .executeScript("arguments[0].dispatchEvent(new Event('sl-change'))", getWebElement());
    }
}
