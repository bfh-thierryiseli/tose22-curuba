package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SlSelect extends ElementBase implements WriteableElement {
    @Override
    public String getValue() {
        return getWebElement().getAttribute("value");
    }

    @Override
    public void setValue(String value) {
        getWebElement().click();
        List<WebElement> menuItems = getWebElement().findElements(By.cssSelector("sl-menu-item"));
        for (WebElement menuItem : menuItems) {
            if (menuItem.getText().equals(value)) {
                menuItem.click();
            }
        }
    }
}
