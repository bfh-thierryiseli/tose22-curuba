package ch.clicknbook.bdd.base.web_elements;

public interface ReadableElement extends TypedWebElement {
    String getValue();
}
