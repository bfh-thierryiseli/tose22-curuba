package ch.clicknbook.bdd.base;

import ch.clicknbook.bdd.base.web_elements.*;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfAllElementsLocatedBy;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class BasePageObject {
    protected WebDriver webDriver;

    @Getter
    @Setter
    private int waitToFindElementInSeconds = 5;

    public BasePageObject(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    protected WebElement findElement(String cssSelector) {
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(waitToFindElementInSeconds));
        return wait.until(presenceOfElementLocated(By.cssSelector(cssSelector)));
    }

    protected List<WebElement> findElements(String cssSelector) {
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(waitToFindElementInSeconds));
        return wait.until(presenceOfAllElementsLocatedBy(By.cssSelector(cssSelector)));
    }

    public <T extends TypedWebElement> T findTypedElement(String cssSelector) {
        WebElement element = findElement(cssSelector);
        ElementBase specificElement = mapToElementBase(element);
        specificElement.setBasePageObject(this);
        specificElement.setWebElement(element);
        return (T) specificElement;
    }

    public <T extends TypedWebElement> List<T> findTypedElements(String cssSelector) {
        List<WebElement> allWebElements = findElements(cssSelector);

        ElementBase elementBase;
        List<T> returnedElements = new ArrayList<>();
        for (WebElement webElement : allWebElements) {
            elementBase = mapToElementBase(webElement);
            elementBase.setBasePageObject(this);
            elementBase.setWebElement(webElement);
            returnedElements.add((T) elementBase);
        }

        return returnedElements;
    }

    public ElementBase mapToElementBase(WebElement webElement) {
        final String tagName = webElement.getTagName().toLowerCase();
        ElementBase specificElement;
        switch (tagName) {
            case "sl-button":
                specificElement = new HtmlButton();
                break;
            case "sl-rating":
                specificElement = new SlRating();
                break;
            case "sl-input":
                specificElement = new SlInput();
                break;
            case "sl-textarea":
                specificElement = new SlTextarea();
                break;
            case "sl-select":
                specificElement = new SlSelect();
                break;
            case "input":
                String type = webElement.getAttribute("type");
                switch (type) {
                    case "submit":
                        specificElement = new HtmlButton();
                        break;
                    default:
                        specificElement = new HtmlInput();
                }
                break;
            default:
                specificElement = new HtmlTextDisplay();
                break;
        }
        return specificElement;
    }

    public WebDriver getWebDriver() {
        return this.webDriver;
    }

}
