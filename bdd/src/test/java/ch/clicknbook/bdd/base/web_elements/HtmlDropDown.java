package ch.clicknbook.bdd.base.web_elements;

public class HtmlDropDown extends ElementBase implements WriteableElement {

    @Override
    public String getValue() {
        return getSeleniumSelect().getFirstSelectedOption().getText();
    }

    @Override
    public void setValue(String value) {
        getSeleniumSelect().selectByVisibleText(value);
    }

    public org.openqa.selenium.support.ui.Select getSeleniumSelect() {
        return new org.openqa.selenium.support.ui.Select(getWebElement());
    }

}
