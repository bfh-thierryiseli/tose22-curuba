package ch.clicknbook.bdd.base.web_elements;

public interface ClickableElement extends ReadableElement {
    void click();
}
