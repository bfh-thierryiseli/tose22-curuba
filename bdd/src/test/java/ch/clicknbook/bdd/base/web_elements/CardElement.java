package ch.clicknbook.bdd.base.web_elements;

public interface CardElement extends TypedWebElement {
    String getTitle();
    String getDescription();
}
