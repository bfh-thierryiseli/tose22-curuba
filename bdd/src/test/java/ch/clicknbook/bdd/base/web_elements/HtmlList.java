package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.stream.Collectors;

public class HtmlList extends ElementBase implements ReadableElement {

    @Override
    public String getValue() {
        return getWebElement().getText();
    }

    public java.util.List<WebElement> getElements() {
        return getWebElement().findElements(By.tagName("li"));
    }

    public java.util.List<String> getListTexts() {
        return getElements()
                .stream()
                .map(element -> element.getText())
                .collect(Collectors.toList());
    }
}
