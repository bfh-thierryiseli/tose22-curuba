package ch.clicknbook.bdd.base.web_elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class HtmlRadioButton extends ElementBase implements WriteableElement {
    @Override
    public String getValue() {
        return getValue(getWebElement());
    }

    @Override
    public void setValue(String value) {
        setValue(getWebElement(), value);
    }

    private void setValue(WebElement element, String value) {
        boolean inputNotSelected = !element.isSelected();
        Actions actions = new Actions(this.getBasePageObject().getWebDriver());
        if (inputNotSelected && value.equalsIgnoreCase("selected")) {
            actions.moveToElement(element).click().build().perform();
        }
    }

    private String getValue(WebElement element) {
        return element.isSelected() ? "selected" : "unselected";
    }
}
