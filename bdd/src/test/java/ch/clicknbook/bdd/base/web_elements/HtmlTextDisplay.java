package ch.clicknbook.bdd.base.web_elements;

public class HtmlTextDisplay extends ElementBase implements ReadableElement {

    @Override
    public String getValue() {
        return getWebElement().getText();
    }
}
