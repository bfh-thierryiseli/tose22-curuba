package ch.clicknbook.bdd.base.web_elements;

public class HtmlButton extends ElementBase implements ReadableElement, ClickableElement {
    @Override
    public String getValue() {
        return getWebElement().getText();
    }

    @Override
    public void click() {
        getWebElement().click();
    }
}
