package ch.clicknbook.bdd.base;

import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ApiService {
    private static final String HEADER_BEARER_PREFIX = "Bearer ";

    protected final WebTarget webTarget;
    protected final String resource;
    protected final String accessToken;

    public ApiService(WebTarget provider, String resource) {
        this.webTarget = provider;
        this.resource = resource;
        this.accessToken = null;
    }

    public ApiService(WebTarget provider, String resource, String accessToken) {
        this.webTarget = provider;
        this.resource = resource;
        this.accessToken = accessToken;
    }

    public Response post(JsonObject object) {
        return prepareRequest(webTarget.path(resource)
                .request())
                .post(Entity.json(object));
    }

    public Response get() {
        return prepareRequest(
                webTarget.path(resource)
                        .request())
                .get();
    }

    public Response getWithQueryParams(HashMap<String, String> params) {
        WebTarget webTarget = this.webTarget.path(resource);
        if (params != null) {
            for (Map.Entry<String, String> set : params.entrySet()) {
                if (set.getValue().contains(",")) {
                    List<String> split = Arrays.stream(set.getValue().split(",")).collect(Collectors.toList());
                    for (String substr : split) {
                        webTarget = webTarget.queryParam(set.getKey(), substr);
                    }
                } else {
                    webTarget = webTarget.queryParam(set.getKey(), set.getValue());
                }
            }
        }
        return prepareRequest(webTarget.request()).get();
    }

    public Response get(long id) {
        return get(String.valueOf(id));
    }

    public Response get(String id) {
        return prepareRequest(
                webTarget.path(resource)
                        .path(id)
                        .request())
                .get();
    }

    public Response getByLocation(String location) {
        return prepareRequest(ClientBuilder
                .newBuilder()
                .build()
                .target(location)
                .request())
                .get();
    }

    public Response put(JsonObject object) {
        return put("id", object);
    }

    public Response put(String propertyName, JsonObject object) {
        return prepareRequest(
                webTarget.path(resource)
                        .path(object.get(propertyName).toString())
                        .request())
                .put(Entity.json(object));
    }

    public Response delete(long id) {
        return delete(String.valueOf(id));
    }

    public Response delete(String id) {
        return prepareRequest(
                webTarget.path(resource)
                        .path(id)
                        .request())
                .delete();
    }

    protected Invocation.Builder prepareRequest(Invocation.Builder builder) {
        if (this.accessToken != null && !this.accessToken.isEmpty()) {
            builder = builder.header(HttpHeaders.AUTHORIZATION, HEADER_BEARER_PREFIX + accessToken);
        }
        return builder;
    }
}
