package ch.clicknbook.bdd.artist.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Artist {
    @JsonAlias({"name"})
    private String name;
    @JsonAlias({"bereich"})
    private String section;
    @JsonAlias({"beschreibung"})
    private String description;
    @JsonAlias({"email"})
    private String email;
    @JsonAlias({"strasseUndNummer"})
    private String street;
    @JsonAlias({"postleitzahl"})
    private String postalCode;
    @JsonAlias({"ort"})
    private String town;
    @JsonAlias({"bewertung"})
    private String rating;
    @JsonAlias({"stichwörter"})
    private String tags;
}
