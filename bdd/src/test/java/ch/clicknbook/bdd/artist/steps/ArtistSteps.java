package ch.clicknbook.bdd.artist.steps;

import ch.clicknbook.bdd.DataserviceService;
import ch.clicknbook.bdd.User;
import ch.clicknbook.bdd.artist.dtos.Artist;
import ch.clicknbook.bdd.artist.dtos.UserArtist;
import ch.clicknbook.bdd.base.ApiService;
import io.cucumber.java.de.Angenommen;

import javax.json.*;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

public class ArtistSteps {
    public static final String SECTION = "sections";
    public static final String ARTISTS_PUBLIC = "artists/public";

    private final ApiService serviceSections;
    private final ApiService dataserviceInsert;
    private final ApiService artistPublicService;

    public ArtistSteps() {
        WebTarget backend = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("BACKEND_URL"));
        WebTarget dataservice = ClientBuilder
                .newBuilder()
                .build()
                .target(System.getenv("DATASERVICE_URL"));
        this.serviceSections = new ApiService(
                backend,
                SECTION);
        this.dataserviceInsert = new ApiService(
                dataservice,
                DataserviceService.INSERTS);
        this.artistPublicService = new ApiService(
                backend,
                ARTISTS_PUBLIC);
    }

    @Angenommen("es exisitieren folgende Künstler")
    public void esExisitierenFolgendeKünstler(List<Artist> artists) {
        JsonArray sections = serviceSections.get().readEntity(JsonArray.class);
        for (Artist artist : artists) {
            BigInteger sectionId = sections
                    .stream()
                    .map(JsonValue::asJsonObject)
                    .filter(x -> x.getString("name").equals(artist.getSection()))
                    .findFirst()
                    .get()
                    .getJsonNumber("id")
                    .bigIntegerValue();
            JsonObjectBuilder data = Json.createObjectBuilder()
                    .add("name", artist.getName())
                    .add("email", artist.getName().replace(" ", ".") + "@clicknbook.ch")
                    .add("section_id", sectionId);
            if (artist.getRating() != null) {
                data.add("rating", Integer.parseInt(artist.getRating()));
            }
            dataserviceInsert.post(
                    Json.createObjectBuilder()
                            .add("table", "artist")
                            .add("data", data.build())
                            .build()
            );
        }
    }

    @Angenommen("es existieren folgende Künstlerzuweisungen")
    public void esExistierenFolgendeKünstlerzuweisungen(List<UserArtist> userArtists) {
        for (UserArtist userArtist : userArtists) {
            HashMap<String, String> queryParams = new HashMap();
            queryParams.put("nameDescription", userArtist.getArtistName());
            JsonObject artist = artistPublicService
                    .getWithQueryParams(queryParams)
                    .readEntity(JsonArray.class)
                    .get(0)
                    .asJsonObject();
            User user = User.valueOf(userArtist.getUser());
            dataserviceInsert.post(
                    Json.createObjectBuilder()
                            .add("table", "user_artist")
                            .add("data", Json.createObjectBuilder()
                                    .add("username", user.getUsername())
                                    .add("artist_id", artist.getJsonNumber("id").bigIntegerValue())
                                    .build())
                            .build());
            dataserviceInsert.post(
                    Json.createObjectBuilder()
                            .add("table", "user_setting")
                            .add("data", Json.createObjectBuilder()
                                    .add("username", user.getUsername())
                                    .add("default_artist_id", artist.getJsonNumber("id").bigIntegerValue())
                                    .build())
                            .build());
        }
    }
}
