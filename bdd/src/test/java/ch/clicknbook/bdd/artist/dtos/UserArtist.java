package ch.clicknbook.bdd.artist.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserArtist {
    @JsonAlias({"künstlername"})
    private String artistName;
    @JsonAlias({"benutzer"})
    private String user;
}
