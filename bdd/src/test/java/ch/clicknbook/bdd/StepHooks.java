package ch.clicknbook.bdd;

import ch.clicknbook.bdd.base.browsers.ConfiguredChrome;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class StepHooks {
    protected final static Logger LOGGER = Logger.getLogger(StepHooks.class.getName());

    public WebDriver browserDriver;
    public String baseUrl;
    public Scenario scenario;

    @Before
    public void beforeScenario(Scenario scenario) throws InterruptedException {
        DataserviceService dataserviceService = new DataserviceService();
        dataserviceService.cleanup();
        dataserviceService.loadMasterData();
        this.scenario = scenario;
        baseUrl = System.getenv("FRONTEND_URL");
        switch (System.getenv("BROWSER")) {
            case "LocalChrome":
                ChromeDriverManager.chromedriver().setup();
                browserDriver = ConfiguredChrome.getChrome();
                break;
            case "RemoteChrome":
                browserDriver = ConfiguredChrome.getRemoteChrome();
                break;
        }
    }

    @After
    public void afterScenario() {
        browserDriver.quit();
        browserDriver = null;
    }

    @AfterStep
    public void afterStep() {
        if (System.getenv("SCREENSHOTS") != null && System.getenv("SCREENSHOTS").equals("true")) {
            scenario.attach(((TakesScreenshot) browserDriver).getScreenshotAs(OutputType.BYTES), "image/jpeg", "Screenshot");
        }
    }
}
