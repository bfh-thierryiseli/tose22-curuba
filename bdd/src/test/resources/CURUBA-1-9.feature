# language: de
@CURUBA-1-9
Funktionalität: Künstler kontaktieren
  Als Unternehmer*in
  möchte ich mit einem Künstler Kontakt aufnehmen können
  um weitere Details zum Angebot und Buchung in Erfahrung zu bringen

  Szenario: Künstler erfogreich kontaktieren
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Comedy  |
    Und ich befinde mich auf der Kontaktseite für "Kaya Yanar"
    Wenn ich folgende Kontaktanfrage sende:
      | Name       | Email                    | Strasse und Nummer | Postleitzahl | Ort  | Fragen oder Anliegen                 |
      | Max Muster | max.muster@clicknbook.ch | Bernstrasse 1      | 9999         | Bern | Wie kann es so sein, dass es so ist? |
    Dann sehe ich die Erfolgsmeldung "Kontaktanfrage wurde erfolgreich versendet."
    Und erhält der Künstler "Kaya Yanar" die folgende Email
      | Betreff                              | Inhalt                                            |
      | Neue Kontaktanfrage von:  Max Muster | Nachricht:\nWie kann es so sein, dass es so ist?? |