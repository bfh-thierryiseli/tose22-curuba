# language: de
@CURUBA-1-4
Funktionalität: Künstler buchen
  Als Unternehmer*in
  will ich ein Formular zur Buchung von Künstler*innen
  um diese für einen Auftritt zu buchen

  Szenario: Künstler erfogreich buchen
    Angenommen es exisitieren folgende Künstler
      | Name            | Section |
      | Robbie Williams | Musik   |
    Und ich befinde mich auf der Buchungsseite für "Robbie Williams"
    Wenn ich folgende Reservation vornehme:
      | Name       | Email                    | Strasse und Nummer (Veranstaltungsort) | Postleitzahl (Veranstaltungsort) | Ort (Veranstaltungsort) | Reservationdatum | Von   | Bis   |
      | Max Muster | max.muster@clicknbook.ch | Bernstrasse 1                          | 9999                             | Bern                    | heute+2tag       | 20:00 | 22:00 |
    Dann sehe ich die Reservetionsdetails für "Robbie Williams"