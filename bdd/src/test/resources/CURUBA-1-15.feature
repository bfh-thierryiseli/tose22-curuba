# language: de
@CURUBA-1-15
Funktionalität: Künstler mit Schlagwörter ergänzen
  Als Künstler*in
  möchte ich in meinem Profil Schlagworte definieren können
  um bei der Suche besser gefunden zu werden

  Szenario: Künstler erfogreich bewerten
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Comedy  |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und ich bin als "Test" eingeloggt
    Und ich befinde mich auf den Künstlerprofilen
    Wenn ich den Künstler "Kaya Yanar" mit folgenden Stichwörter ergänze
      | Name               |
      | Humor              |
      | Sprachengenie      |
      | DialekteNachmachen |
    Dann sehe ich bei "Kaya Yanar" die definierten Stichwörter
      | Name               |
      | Humor              |
      | Sprachengenie      |
      | DialekteNachmachen |