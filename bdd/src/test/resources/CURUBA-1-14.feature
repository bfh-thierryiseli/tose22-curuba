# language: de
@CURUBA-1-14
Funktionalität: Künstler nach der Reservation bewerten
  Als Unternehmer*in
  möchte ich den Künstler bewerten können
  um mein Feedback zu diesem zu geben

  Szenario: Künstler erfogreich bewerten
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Comedy  |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und es existieren folgende Reservationen
      | Name       | Email                    | Strasse und Nummer (Veranstaltungsort) | Postleitzahl (Veranstaltungsort) | Ort (Veranstaltungsort) | Reservationdatum | Von   | Bis   | Status | Künstler   |
      | Max Muster | max.muster@clicknbook.ch | Bernstrasse 1                          | 9999                             | Bern                    | heute-3tag       | 20:00 | 23:00 | 2      | Kaya Yanar |
    Und ich befinde mich auf der Maske für die Bewertung von "Kaya Yanar"
    Wenn ich die Bewertung mit folgenden Informationen ausfülle
      | Text           | Bewertung |
      | Das ganz okay! | 4         |
    Dann sehe ich die Erfolgsinfo "Bewertung für Kaya Yanar erfolgreich abgegeben."
