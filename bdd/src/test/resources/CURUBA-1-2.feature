# language: de
@CURUBA-1-2
Funktionalität: Einstiegsmöglichkeit auswählen
  Als Künstler*in oder Unternehmer*in
  will ich eine Einstiegsmöglichkeit haben
  um zwischen der Oberfläche für Künstler*innen oder Unternehmer*innen zu wählen

  Szenario: Als Unternehmer/Interessent erfolgreich Einstiegsmöglichkeit aufrufen
    Angenommen ich befinde mich auf der Hauptseite
    Wenn ich als "Ich bin Interessent" einsteige
    Dann sehe ich die Seite mit dem Titel "Finden Sie den passenden Künstler!"

  Szenario: Als Künstler erfolgreich Einstiegsmöglichkeit aufrufen
    Angenommen ich befinde mich auf der Hauptseite
    Wenn ich als "Ich bin Künstler" einsteige
    Dann sehe ich die Seite mit dem Titel "Künstler*in mit Leidenschaft?"