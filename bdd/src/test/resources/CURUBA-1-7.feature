# language: de
@CURUBA-1-7
Funktionalität: Unternehmens-/Kundenprofil erstellen
  Als Unternehmer*in
  möchte ich ein Benutzerprofil erstellen können, wo ich mittels eines standardisierten Formulars mit Dropdownlisten als auch mittels Freitext meine Künstlerpräferenzen und Kontaktdaten erfassen kann
  um bei zukünftiger Nutzung der Plattform meine Angaben nicht erneut eingeben zu müssen

  Szenario: Unternehmens-/Kundenprofil erfogreich erstellen
    Angenommen ich bin als "Test" eingeloggt
    Und ich befinde mich auf der Maske für die Erstellung eines Kundenprofil
    Wenn ich ein Kundenprofil mit folgenden Informationen erstelle
      | Name      | Präferenz | Beschreibung | Email        | Strasse und Nummer | Postleitzahl | Ort  |
      | Kunden AG | Musik     | KMU          | kunde@ag.com | Kundenstrasse 18   | 9999         | Bern |
    Dann sehe ich das Kundenprofil
      | Name      |
      | Kunden AG |