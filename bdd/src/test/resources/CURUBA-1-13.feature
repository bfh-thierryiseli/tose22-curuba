# language: de
@CURUBA-1-14
Funktionalität: Künstler nach der Reservation bewerten
  Als Benutzer*in
  möchte ich unangemessene Kommentare in Bewertungen melden können
  um zu verhindern dass ein Künstler-Image geschädigt wird.

  Szenario: Unangemessene Kommentare erfogreich melden
    Angenommen es exisitieren folgende Künstler
      | Name       | Section | Bewertung |
      | Kaya Yanar | Comedy  | 4         |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und es existieren folgende Reservationen
      | Name       | Email                    | Strasse und Nummer (Veranstaltungsort) | Postleitzahl (Veranstaltungsort) | Ort (Veranstaltungsort) | Reservationdatum | Von   | Bis   | Status | Künstler   |
      | Max Muster | max.muster@clicknbook.ch | Bernstrasse 1                          | 9999                             | Bern                    | heute-3tag       | 20:00 | 23:00 | 2      | Kaya Yanar |
    Und existiert folgende Bewertung
      | Text      | Bewertung | Name       | Künstlername |
      | Bullshit! | 4         | Max Muster | Kaya Yanar   |
    Wenn ich die Bewertungen von "Kaya Yanar" betrachte
    Und die Bewertung "Bullshit!" melde
    Dann sehe ich die Erfolgsinfo "Bewertung wurde gemeldet, der Plattformbetreiber wird darüber informiert."

