# language: de
@CURUBA-1-8
Funktionalität: Als Künstler Buchungsübersicht erhalten
  Als Künstler*in
  möchte ich eine Übersicht der vergangenen Buchungen erhalten
  um diese bei Bedarf einsehen zu können

  Szenario: Buchungsübersicht erfogreich sehen
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Comedy  |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und es existieren folgende Reservationen
      | Name       | Email                    | Strasse und Nummer (Veranstaltungsort) | Postleitzahl (Veranstaltungsort) | Ort (Veranstaltungsort) | Reservationdatum | Von   | Bis   | Status | Künstler   |
      | Max Muster | max.muster@clicknbook.ch | Bernstrasse 1                          | 9999                             | Bern                    | 01.06.2022       | 20:00 | 22:00 | 2      | Kaya Yanar |
    Und ich bin als "Test" eingeloggt
    Wenn ich die Reservationen als Künstler einsehe
    Dann sehe ich die vergangene Reservation
      | Name       |
      | Max Muster |