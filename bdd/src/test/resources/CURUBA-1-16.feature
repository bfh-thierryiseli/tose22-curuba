# language: de
@CURUBA-1-16
Funktionalität: Künstler mit Schlagwort suchen
  Als Unternehmer*in
  möchte ich Künstler*innen auch mittels Freitexteingabe suchen können
  um mein Suchergebnis zu optimieren

  Szenario: Künstler erfogreich mit Schlagwort suchen
    Angenommen es exisitieren folgende Künstler
      | Name          | Section |
      | Kaya Yanar    | Comedy  |
      | Vincent Weiss | Musik   |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und ich bin als "Test" eingeloggt
    Und ich befinde mich auf den Künstlerprofilen
    Wenn ich den Künstler "Kaya Yanar" mit folgenden Stichwörter ergänze
      | Name               |
      | Humor              |
      | Sprachengenie      |
      | DialekteNachmachen |
    Angenommen ich befinde mich auf der Künstlerübersicht
    Wenn ich nach "Sprachengenie" suche
    Dann sehe ich das Künstlerprofil
      | Name       |
      | Kaya Yanar |
    Und sehe ich den Künstler nicht
      | Name          |
      | Vincent Weiss |