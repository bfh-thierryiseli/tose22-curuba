# language: de
@CURUBA-1-3
Funktionalität: Künstlerübersicht erhalten
  Als Unternehmer*in
  will ich eine tabellarische Übersicht der Künstler
  um das für mich passende Künstlerangebot zu finden

  Szenario: Künstlerübersicht erfogreich sehen
    Angenommen es exisitieren folgende Künstler
      | Name            | Section |
      | Robbie Williams | Musik   |
    Und ich befinde mich auf der Künstlerübersicht
    Dann sehe ich den Künstler
      | Name            |
      | Robbie Williams |