# language: de
@CURUBA-1-5
Funktionalität: Künstlerangebote filtern
  Als Unternehmer*in
  will ich eine einfache Suche der Künstlerangebote mit vorbereiteten Filtern haben
  um die Übersicht auf meine Bedürfnisse einschränken zu können

  Szenario: Künstler erfogreich suchen
    Angenommen es exisitieren folgende Künstler
      | Name            | Section |
      | Robbie Williams | Musik   |
      | Peter Jackson   | Musik   |
    Und ich befinde mich auf der Künstlerübersicht
    Wenn ich nach "Robbie Williams" suche
    Dann sehe ich den Künstler
      | Name            |
      | Robbie Williams |
    Und sehe ich den Künstler nicht
      | Name          |
      | Peter Jackson |