# language: de
@CURUBA-1-6
Funktionalität: Künstlerprofil erstellen
  Als Künstler*in
  möchte ich ein Benutzerprofil erstellen können, wo ich mittels eines standardisierten Formulars mit Dropdownlisten als auch mittels Freitext meine künstlerischen Qualitäten und Kontaktdaten erfassen kann
  um bei zukünftiger Nutzung der Plattform meine Angaben nicht erneut eingeben zu müssen

  Szenario: Künstlerprofil erfogreich erstellen
    Angenommen ich bin als "Test" eingeloggt
    Und ich befinde mich auf der Maske für die Erstellung eines Künstlerprofils
    Wenn ich ein Künstlerprofil mit folgenden Informationen erstelle
      | Name       | Bereich | Beschreibung   | Email        | Strasse und Nummer | Postleitzahl | Ort    |
      | Kaya Yanar | Comedy  | Was guckst du? | info@kaya.tv | Kayastrasse 18     | 9999         | Zürich |
    Dann sehe ich das Künstlerprofil
      | Name       |
      | Kaya Yanar |