# language: de
@CURUBA-1-11
Funktionalität: Verfügbarkeit als Künstlers erfassen
  Als Künstler*in
  möchte ich per Angabe von Verfügbarkeitsdaten und -zeiten meine Verfügbarkeiten erfassen können
  um bei der Verfügbarkeitsprüfung korrekt in den Suchergebnissen zu erscheinen

  Szenario: Verfügbarkeit als Künstlers erfogreich erfassen
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Comedy  |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und ich bin als "Test" eingeloggt
    Und ich befinde mich auf der Maske fürs Eintragen der Verfügbarkeiten
    Wenn ich Verfügbarkeit für die nächsten zwei Wochen eintrage
    Dann sehe ich die Erfolgsinfo "Verfügbarkeit von (.*) bis (.*) erfolgreich eingetragen."

  Szenario: Verfügbarkeit als Künstlers erfogreich löschen
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Musik   |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und ich bin als "Test" eingeloggt
    Und ich befinde mich auf der Maske fürs Eintragen der Verfügbarkeiten
    Wenn ich Verfügbarkeit für die nächsten zwei Wochen lösche
    Dann sehe ich die Erfolgsinfo "Verfügbarkeit von (.*) bis (.*) erfolgreich gelöscht."