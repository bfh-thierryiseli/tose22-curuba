# language: de
@CURUBA-1-12
Funktionalität: Nach verfügbaren Künstler filtern
  Als Unternehmer*in
  möchte ich bei der Suchen nach geeigneten Künstler*innen nach deren Verfügbarkeit filtern können
  um sofort verfügbare Künstler*innen zu sehen

  Szenario: Verfügbarer Künstler erfogreich finden
    Angenommen es exisitieren folgende Künstler
      | Name       | Section |
      | Kaya Yanar | Comedy  |
    Und es existieren folgende Künstlerzuweisungen
      | Künstlername | Benutzer |
      | Kaya Yanar   | Test     |
    Und es existieren folgende Reservationen
      | Name       | Email                    | Strasse und Nummer (Veranstaltungsort) | Postleitzahl (Veranstaltungsort) | Ort (Veranstaltungsort) | Reservationdatum | Von   | Bis   | Status | Künstler   |
      | Max Muster | max.muster@clicknbook.ch | Bernstrasse 1                          | 9999                             | Bern                    | heute+1tag       | 00:00 | 23:59 | 0      | Kaya Yanar |
    Und ich bin als "Test" eingeloggt
    Und ich befinde mich auf der Künstlerübersicht
    Wenn ich nach dem Datum "heute+1tag" filtere
    Dann sehe ich den Künstler
      | Name       |
      | Kaya Yanar |
