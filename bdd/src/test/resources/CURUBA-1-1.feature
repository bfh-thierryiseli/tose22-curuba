# language: de
@CURUBA-1-1
Funktionalität: Platform aufrufen
  Als Künstler*in oder Unternehmer*in
  will ich die Plattform aufrufen
  um Zugriff auf diese zu erhalten

  Szenario: Platform erfogreich aufrufen
    Angenommen ich befinde mich auf der Hauptseite
    Dann sehe ich den Seiteneinstieg mit dem Titel "click & book"